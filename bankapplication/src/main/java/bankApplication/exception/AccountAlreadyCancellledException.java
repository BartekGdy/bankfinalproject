package bankApplication.exception;

public class AccountAlreadyCancellledException extends RuntimeException {

    public AccountAlreadyCancellledException(String message) {
        super(message);
    }
}
