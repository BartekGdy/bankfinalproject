package bankApplication.mapper;

import org.springframework.stereotype.Component;
import sharedData.mapper.Mapper;
import sharedData.model.CustomerAppAddress;
import sharedData.model.dto.CustAppAddressDto;


@Component("address-app")
public class AddressMapper implements Mapper<CustomerAppAddress,CustAppAddressDto> {

    public CustomerAppAddress mapDto(CustAppAddressDto dto) {
        CustomerAppAddress entity = new CustomerAppAddress();
        entity.setPremise(dto.getPremise());
        entity.setStreet(dto.getStreet());
        entity.setTown(dto.getTown());
        entity.setPostCode(dto.getPostCode());
        entity.setAddressType(dto.getAddressType());
        entity.setTimeAtAddress(dto.getTimeAtAddress());
        return entity;
    }


    public CustAppAddressDto mapEntity(CustomerAppAddress entity) {
        CustAppAddressDto dto = new CustAppAddressDto();
        dto.setPremise(entity.getPremise());
        dto.setStreet(entity.getStreet());
        dto.setTown(entity.getTown());
        dto.setPostCode(entity.getPostCode());
        dto.setAddressType(entity.getAddressType());
        dto.setTimeAtAddress(entity.getTimeAtAddress());
        return dto;
    }
}
