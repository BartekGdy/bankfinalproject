package bankApplication.mapper;

import sharedData.model.CustomerAppAddress;
import sharedData.converter.LocalDateAttributeConverter;
import sharedData.mapper.Mapper;
import bankApplication.model.Customer;
import bankApplication.model.dto.input.CustomerDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;
import sharedData.model.dto.CustAppAddressDto;

import java.util.ArrayList;
import java.util.List;

@Component("customer")
public class CustomerMapper implements Mapper<Customer, CustomerDto> {

    @Autowired
    @Qualifier("address-app")
    private Mapper<CustomerAppAddress, CustAppAddressDto> mapperAddress;

    @Autowired
    LocalDateAttributeConverter dateConverter;

    @Override
    public CustomerDto mapEntity(Customer entity) {
        CustomerDto dto = new CustomerDto();
        dto.setEmail(entity.getEmail());
        dto.setFirstname(entity.getFirstname());
        dto.setLastname(entity.getLastname());
        dto.setPassword(entity.getPassword());
        dto.setPhone(entity.getPhone());
        dto.setUsername(entity.getUsername());
        List<CustomerAppAddress> customerAppAddresses = entity.getCustomerAppAddresses();
        List<CustAppAddressDto> addressesDto = new ArrayList<>();
        for (CustomerAppAddress customerAppAddress : customerAppAddresses) {
            addressesDto.add(mapperAddress.mapEntity(customerAppAddress));
        }
        dto.setAddresses(addressesDto);
        dto.setGender(entity.getGender());
        dto.setDateOfBirth(dateConverter.convert(entity.getDateOfBirth()));
        return dto;
    }

    @Override
    public Customer mapDto(CustomerDto dto) {
        Customer entity = new Customer();
        entity.setEmail(dto.getEmail());
        entity.setFirstname(dto.getFirstname());
        entity.setLastname(dto.getLastname());
        entity.setPassword(dto.getPassword());
        entity.setPhone(dto.getPhone());
        entity.setUsername(dto.getUsername());
        List<CustAppAddressDto> addressesDto = dto.getAddresses();
        List<CustomerAppAddress> customerAppAddresses = new ArrayList<>();
        for (CustAppAddressDto custAppAddressDto : addressesDto) {
            customerAppAddresses.add(mapperAddress.mapDto(custAppAddressDto));
        }
        entity.setCustomerAppAddresses(customerAppAddresses);
        entity.setGender(dto.getGender());
        entity.setDateOfBirth(dateConverter.convert(dto.getDateOfBirth()));
        return entity;
    }

}
