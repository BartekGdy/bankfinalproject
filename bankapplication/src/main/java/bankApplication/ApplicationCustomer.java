package bankApplication;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@SpringBootApplication
@ComponentScan(basePackages = "bankApplication")
@ComponentScan(basePackages = "sharedData")
@EnableJpaRepositories
@EnableTransactionManagement
//@EntityScan(basePackageClasses = { ApplicationCustomer.class, Jsr310JpaConverters.class })
@EntityScan({"bankApplication","sharedData"})
public class ApplicationCustomer {

	public static void main(final String... args) {
		new SpringApplication(ApplicationCustomer.class).run(args);
	}

	// po uruchomieniu aplikacji zajrzyj na
	// http://localhost:8080/swagger-ui.html
	// dokumentacja spring security
	// https://spring.io/guides/gs/securing-web/
}
