package bankApplication.model.service;

import sharedData.model.SavingApplication;

import java.util.List;
import java.util.Optional;

public interface SavingAccService {

    Long applyForSavingAccount(String customerName, SavingApplication application);

    List<SavingApplication> getAllSavingAppsByCustomer(String customerName);
    Optional<SavingApplication> findApplication(Long applicaionId);
}
