package bankApplication.model.service;

import bankApplication.exception.UnauthorizedExcetion;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsChecker;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import sharedData.encryption.RSA;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.ws.http.HTTPException;
import java.util.Optional;

@Service
public class TokenAuthenticationService {

    public static final String AUTH_HEADER_NAME = "Authorization";
    public static final String API_KEY_HEADER_NAME = "api_key";
    public static final String MAIN_JWT_HEADER_NAME = "mainJwt";

    private static final Logger LOG = LoggerFactory.getLogger(TokenAuthenticationService.class);

    private final CustomerService customerService;
    private final UserDetailsChecker userDetailsChecker;
    private final RSA rsa;

    @Autowired
    public TokenAuthenticationService(final CustomerService customerService, final UserDetailsChecker userDetailsChecker, RSA rsa) {
        this.customerService = customerService;
        this.userDetailsChecker = userDetailsChecker;
        this.rsa = rsa;
    }

    public Authentication getAuthentication(final HttpServletRequest httpRequest,
                                            final HttpServletResponse httpResponse) {

        final Optional<String> currentUserToken = getCurrentUserToken(httpRequest);

        if (currentUserToken.isPresent()) {
            final UserDetails user = parseToken(currentUserToken.get());

            if (user != null) {
                userDetailsChecker.check(user);
                return new UsernamePasswordAuthenticationToken(user.getUsername(), user.getPassword(),
                        user.getAuthorities());
            }
        } else {
            LOG.warn("Auth Token was not found in " + API_KEY_HEADER_NAME + " nor in " + AUTH_HEADER_NAME);
        }
        return null;
    }

    private UserDetails parseToken(String token) {
        String[] usernameAndPassword = token.split("_");
        return customerService.loadUserByUsername(usernameAndPassword[0]);
    }

    private Optional<String> getCurrentUserToken(final HttpServletRequest request) {
        String token = request.getHeader(API_KEY_HEADER_NAME);
        if (token!=null && token.matches("\\d+")) {
            token = rsa.decrypt(token);
        }else {
            token=null;
        }
        if (StringUtils.isEmpty(token)) {
            token = request.getHeader(AUTH_HEADER_NAME);
        }

        return StringUtils.isEmpty(token) ? Optional.empty() : Optional.of(token);
    }

    public String authenticate(final Authentication authentication) throws AuthenticationException {
        final String username = (String) authentication.getPrincipal();
        final UserDetails user = customerService.loadUserByUsername(username);

        if (authentication.getCredentials() == null) {
            throw new HTTPException(401);
        }

        else if (!authentication.getCredentials().equals(user.getPassword())) {
            throw new HTTPException(401);
        }

        userDetailsChecker.check(user);

        return rsa.encrypt(createTokenForUser(user));
    }

    private String createTokenForUser(UserDetails user) {
        return user.getUsername() + "_" + user.getPassword();
    }

    public String getUserNameFromToken(final HttpServletRequest request) {
        Optional<String> currentUserToken = getCurrentUserToken(request);
        if (!currentUserToken.isPresent()) {
            throw new UnauthorizedExcetion("You are not authorized to this request");
        }
        return parseToken(currentUserToken.get()).getUsername();
    }
}

