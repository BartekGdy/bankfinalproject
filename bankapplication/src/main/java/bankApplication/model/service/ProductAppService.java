package bankApplication.model.service;

import sharedData.model.ProductApp;

import java.util.List;

public interface ProductAppService {

    List<ProductApp> getAllApplications(String customerName);

    Long cancelApplication(Long applicationId, String userName);
}
