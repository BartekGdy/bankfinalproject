package bankApplication.model.service.implementation;

import bankApplication.exception.CustomerNotFoundException;
import bankApplication.model.service.SavingAccService;
import bankApplication.repository.CustomerDetailsRepository;
import bankApplication.repository.SavingAccApplicationRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import sharedData.model.CustomerDetails;
import sharedData.model.SavingApplication;
import sharedData.model.enums.ProductType;
import sharedData.model.enums.Status;

import java.util.List;
import java.util.Optional;

@Service
public class SavingAccServiceImpl implements SavingAccService {

    @Autowired
    private SavingAccApplicationRepository savingAccRepository;
    @Autowired
    private CustomerDetailsRepository customerRepository;


    @Override
    public Long applyForSavingAccount(String customerName, SavingApplication application) {
        Optional<CustomerDetails> customer = customerRepository.findByUsername(customerName);
        if (customer.isPresent()) {
            application.setCustomer(customer.get());
            application.setProduct(ProductType.SAVING);
            application.setStatus(Status.ACTIVE);
            savingAccRepository.save(application);
            customerRepository.save(customer.get());
            return application.getId();
        }
        throw new CustomerNotFoundException();
    }

    @Override
    @Transactional
    public List<SavingApplication> getAllSavingAppsByCustomer(String customerName) {
        Optional<CustomerDetails> customerOptional = customerRepository.findByUsername(customerName);
        if (customerOptional.isPresent()) {
            CustomerDetails customer = customerOptional.get();
            return savingAccRepository.findAllCustomersAccounts(customer.getId());
        }
        throw new CustomerNotFoundException();
    }

    public Optional<SavingApplication> findApplication(Long applicaionId){
        return savingAccRepository.findById(applicaionId);
    }
}

