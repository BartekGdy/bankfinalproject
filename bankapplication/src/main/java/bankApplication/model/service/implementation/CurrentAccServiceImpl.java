package bankApplication.model.service.implementation;

import bankApplication.exception.CustomerNotFoundException;
import bankApplication.model.service.CurrentAccService;
import bankApplication.repository.CurrentAccApplicationRepository;
import bankApplication.repository.CustomerDetailsRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import sharedData.model.CurrentApplication;
import sharedData.model.CustomerDetails;
import sharedData.model.enums.ProductType;
import sharedData.model.enums.Status;

import java.util.List;
import java.util.Optional;

@Service
public class CurrentAccServiceImpl implements CurrentAccService {

    @Autowired
    private CurrentAccApplicationRepository currentAccRepository;
    @Autowired
    private CustomerDetailsRepository customerRepository;

    @Transactional
    @Override
    public Long applyForCurrentAccount(String customerName, CurrentApplication application) {
        Optional<CustomerDetails> customer = customerRepository.findByUsername(customerName);
        if (customer.isPresent()) {
            application.setCustomer(customer.get());
            application.setProduct(ProductType.CURRENT);
            application.setStatus(Status.ACTIVE);
            currentAccRepository.save(application);
            customerRepository.save(customer.get());
            return application.getId();
        }
        throw new CustomerNotFoundException();
    }


    @Override
    @Transactional
    public List<CurrentApplication> getAllCurrentAppsByCustomer(String customerName) {
        Optional<CustomerDetails> customerOptional = customerRepository.findByUsername(customerName);
        if (customerOptional.isPresent()) {
            CustomerDetails customer = customerOptional.get();
            return currentAccRepository.findAllCustomersAccounts(customer.getId());
        }
        throw new CustomerNotFoundException();
    }

    public Optional<CurrentApplication> findApplication(Long applicaionId){
        return currentAccRepository.findById(applicaionId);
    }
}

