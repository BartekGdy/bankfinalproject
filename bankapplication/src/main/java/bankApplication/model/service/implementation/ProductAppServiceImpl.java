package bankApplication.model.service.implementation;

import bankApplication.exception.AccountAlreadyCancellledException;
import bankApplication.exception.ApplicationNotFoundException;
import bankApplication.exception.CustomerNotFoundException;
import bankApplication.exception.UnauthorizedExcetion;
import bankApplication.model.service.ProductAppService;
import bankApplication.repository.CustomerDetailsRepository;
import bankApplication.repository.ProductAppRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import sharedData.model.CustomerDetails;
import sharedData.model.ProductApp;
import sharedData.model.enums.Status;

import java.util.List;
import java.util.Optional;

@Service
public class ProductAppServiceImpl implements ProductAppService {

    @Autowired
    private ProductAppRepository productAppRepository;
    @Autowired
    private CustomerDetailsRepository customerRepository;

    @Override
    @Transactional
    public List<ProductApp> getAllApplications(String customerName) {
        Optional<CustomerDetails> customerOptional = customerRepository.findByUsername(customerName);
        if (customerOptional.isPresent()) {
            CustomerDetails customer = customerOptional.get();
            return productAppRepository.findAllCustomersAccounts(customer.getId());
        }
        throw new CustomerNotFoundException();
    }

    @Override
    @Transactional
    public Long cancelApplication(Long applicationId, String userName) {
        Optional<ProductApp> appOptional = productAppRepository.findById(applicationId);
        List<ProductApp> userApplications = getAllApplications(userName);
        if (appOptional.isPresent()) {
            ProductApp productApp = appOptional.get();
            if (!userApplications.contains(productApp)) {
                throw new ApplicationNotFoundException();
            }
            if (productApp.getStatus().equals(Status.CANCELLED)) {
                throw new AccountAlreadyCancellledException("ApplicationCustomer id " + productApp.getId() + " is already requested for cancellation");
            }
            productApp.setStatus(Status.CANCELLED);
            productAppRepository.save(productApp);
            return productApp.getId();
        }
        throw new CustomerNotFoundException();
    }


}
