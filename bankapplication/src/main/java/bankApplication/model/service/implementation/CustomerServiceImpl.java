package bankApplication.model.service.implementation;

import bankApplication.model.*;
import bankApplication.model.service.CustomerService;
import bankApplication.repository.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import sharedData.model.CustomerAppAddress;

import javax.transaction.Transactional;
import java.util.*;

@Service
public class CustomerServiceImpl implements CustomerService {

    @Autowired
    private CustomerRepository customerRepository;
    @Autowired
    private AddressRepository addressRepository;

    @Transactional
    @Override
    public Optional<Customer> register(Customer customer) {
        Customer savedCustomer = customerRepository.save(customer);
        if (savedCustomer != null) {
            List<CustomerAppAddress> customerAddresses = customer.getCustomerAppAddresses();
            for (CustomerAppAddress customerAddress : customerAddresses) {
                customerAddress.setCustomer(customer);
                addressRepository.save(customerAddress);
            }
        }
        return Optional.ofNullable(savedCustomer);
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        Optional<Customer> customer = customerRepository.findByUsername(username);
        if (customer.isPresent()) {
            return new org.springframework.security.core.userdetails.User(
                    customer.get().getUsername(),
                    customer.get().getPassword(),
                    true,
                    true,
                    true,
                    true,
                    Collections.emptyList());
        }
        throw new UsernameNotFoundException("Incorrect token");
    }

    @Override
    public Optional<Customer> findById(Long id) {
        return customerRepository.findById(id);
    }

    @Override
    public Optional<Customer> findByUsername(String username) {
        Optional<Customer> customerOptional = customerRepository.findByUsername(username);
        if (customerOptional.isPresent()) {
            return customerOptional;
        }
        return Optional.empty();
    }

    @Override
    public Optional<Customer> editCustomerDetails(String username, Customer edditedCustomer) {
        Optional<Customer> customerOptional = customerRepository.findByUsername(username);
        if (customerOptional.isPresent() && edditedCustomer != null) {
            Customer customer = customerOptional.get();
            customer.setEmail(edditedCustomer.getEmail());
            customer.setFirstname(edditedCustomer.getFirstname());
            customer.setLastname(edditedCustomer.getLastname());
            customer.setPassword(edditedCustomer.getPassword());
            customer.setPhone(edditedCustomer.getPhone());
            List<CustomerAppAddress> customerAddresses = customer.getCustomerAppAddresses();
            List<CustomerAppAddress> edditedAddresses = edditedCustomer.getCustomerAppAddresses();
            for (int i = 0; i < edditedAddresses.size(); i++) {
                customerAddresses.get(i).setAddressType(edditedAddresses.get(i).getAddressType());
                customerAddresses.get(i).setPostCode(edditedAddresses.get(i).getPostCode());
                customerAddresses.get(i).setPremise(edditedAddresses.get(i).getPremise());
                customerAddresses.get(i).setStreet(edditedAddresses.get(i).getStreet());
                customerAddresses.get(i).setTown(edditedAddresses.get(i).getStreet());
                customerAddresses.get(i).setTimeAtAddress(edditedAddresses.get(i).getTimeAtAddress());
            }
            customer.setGender(edditedCustomer.getGender());
            customer.setDateOfBirth(edditedCustomer.getDateOfBirth());
            customerRepository.save(customer);
            return Optional.of(customer);
        }
        return Optional.empty();
    }


}

