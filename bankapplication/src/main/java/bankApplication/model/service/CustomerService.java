package bankApplication.model.service;

import bankApplication.model.*;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Optional;

public interface CustomerService extends UserDetailsService {
    Optional<Customer> register(Customer customer);


    UserDetails loadUserByUsername(String username) throws UsernameNotFoundException;

    Optional<Customer> findById(Long id);

    Optional<Customer> findByUsername(String username);

    Optional<Customer> editCustomerDetails(String username, Customer edditedCustomer);
}
