package bankApplication.model.service;

import sharedData.model.CurrentApplication;

import java.util.List;
import java.util.Optional;

public interface CurrentAccService {

    Long applyForCurrentAccount(String customerName, CurrentApplication application);

    List<CurrentApplication> getAllCurrentAppsByCustomer(String customerName);

    Optional<CurrentApplication> findApplication(Long applicaionId);
}
