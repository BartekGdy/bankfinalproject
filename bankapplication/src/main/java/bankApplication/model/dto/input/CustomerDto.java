package bankApplication.model.dto.input;

import sharedData.model.dto.CustAppAddressDto;
import sharedData.model.enums.Gender;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateDeserializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateSerializer;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.List;

@ApiModel(description = "Customer")
public class CustomerDto implements Serializable{
	@ApiModelProperty(value = "Login")
	@JsonProperty("username")
	@NotNull
	@Size(min = 8,max = 30)
	private String username;
	@ApiModelProperty(value = "Password")
	@JsonProperty("password")
	@NotNull
	@Size(min = 8,max = 30)
	private String password;
	@ApiModelProperty(value = "Name")
	@JsonProperty("firstname")
	@NotNull
	@Size(min = 2,max = 30)
	private String firstname;
	@ApiModelProperty(value = "Surname")
	@JsonProperty("lastname")
	@NotNull
	@Size(min = 2,max = 30)
	private String lastname;
	@ApiModelProperty(value = "DOB",dataType = "java.lang.String", example = "01/01/2001")
	@JsonProperty("DOB")
	@JsonFormat(pattern = "dd/MM/yyyy")
	@JsonDeserialize(using = LocalDateDeserializer.class)
	@JsonSerialize(using = LocalDateSerializer.class)
	private LocalDate dateOfBirth;
	@ApiModelProperty(value = "Gender")
	@JsonProperty("gender")
	@NotNull
	private Gender gender;
	@ApiModelProperty(value = "Email")
	@JsonProperty("email")
	@Email
	private String email;
	@ApiModelProperty(value = "CustomerAppAddress")
	@JsonProperty("addresses")
	private List<CustAppAddressDto> addresses;
	@ApiModelProperty(value = "Phone_Number")
	@JsonProperty("phone")
	@NotEmpty
	private String phone;


	public List<CustAppAddressDto> getAddresses() {
		return addresses;
	}

	public void setAddresses(List<CustAppAddressDto> address) {
		this.addresses = address;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getFirstname() {
		return firstname;
	}

	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}

	public String getLastname() {
		return lastname;
	}

	public void setLastname(String lastname) {
		this.lastname = lastname;
	}

	public LocalDate getDateOfBirth() {
		return dateOfBirth;
	}

	public void setDateOfBirth(LocalDate dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}

	public Gender getGender() {
		return gender;
	}

	public void setGender(Gender gender) {
		this.gender = gender;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

}