package bankApplication.model;

import sharedData.model.CustomerAppAddress;
import sharedData.model.CustomerDetails;
import sharedData.model.enums.Gender;
import org.hibernate.validator.constraints.NotEmpty;

import javax.persistence.*;
import javax.validation.constraints.*;
import java.util.Date;
import java.util.List;

@Entity
public class Customer extends CustomerDetails{


    @NotEmpty
    @Size(min=8, max=30)
    @Column(unique = true)
    private String username;
    @NotEmpty
    @Size(min=8, max=30)
    private String password;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Long getId(){return super.getId();}

    public String getFirstname() {
        return super.getFirstname();
    }

    public void setFirstname(String firstname) {
        super.setFirstname(firstname);
    }

    public String getLastname() {
        return super.getLastname();
    }

    public void setLastname(String lastname) {
        super.setLastname(lastname);
    }

    public Date getDateOfBirth() {
        return super.getDateOfBirth();
    }

    public void setDateOfBirth(Date dateOfBirth) {
        super.setDateOfBirth(dateOfBirth);
    }

    public Gender getGender() {
        return super.getGender();
    }

    public void setGender(Gender gender) {
        super.setGender(gender);
    }

    public String getEmail() {
        return super.getEmail();
    }

    public void setEmail(String email) {
        super.setEmail(email);
    }

    public String getPhone() {
        return super.getPhone();
    }

    public void setPhone(String phone) {
        super.setPhone(phone);
    }

    public List<CustomerAppAddress> getCustomerAppAddresses() {
        return super.getCustomerAppAddresses();
    }

    public void setCustomerAppAddresses(List<CustomerAppAddress> customerAppAddresses) {
        super.setCustomerAppAddresses(customerAppAddresses);
    }
}