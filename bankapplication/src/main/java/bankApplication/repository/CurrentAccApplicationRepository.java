package bankApplication.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import sharedData.model.CurrentApplication;

import java.util.List;
import java.util.Optional;

@Repository
public interface CurrentAccApplicationRepository extends JpaRepository<CurrentApplication, Long> {

    Optional<CurrentApplication> findById(Long id);

    @Query("SELECT c FROM CurrentApplication c WHERE customer_id =:customerId")
    List<CurrentApplication> findAllCustomersAccounts(@Param("customerId") Long id);
}
