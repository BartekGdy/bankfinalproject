package bankApplication.repository;

import bankApplication.model.Customer;
import org.springframework.data.jpa.repository.JpaRepository;
import sharedData.model.ProductApp;

import java.util.List;
import java.util.Optional;

public interface CustomerRepository extends JpaRepository<Customer,Long>{

    Optional<Customer> findByUsername(String username);

    Optional<Customer> findById(Long id);

}
