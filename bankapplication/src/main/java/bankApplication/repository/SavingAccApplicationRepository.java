package bankApplication.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import sharedData.model.SavingApplication;

import java.util.List;
import java.util.Optional;

@Repository
public interface SavingAccApplicationRepository extends JpaRepository<SavingApplication,Long>{

    Optional<SavingApplication> findById(Long id);

    @Query("SELECT s FROM SavingApplication s WHERE customer_id =:customerId")
    List<SavingApplication> findAllCustomersAccounts(@Param("customerId") Long id);

}
