package bankApplication.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import sharedData.model.CustomerDetails;
import sharedData.model.ProductApp;

import java.util.List;
import java.util.Optional;

public interface CustomerDetailsRepository extends JpaRepository<CustomerDetails,Long>{

    @Query("SELECT c FROM CustomerDetails c WHERE c.username =:username")
    Optional<CustomerDetails> findByUsername(@Param("username") String username);

    Optional<CustomerDetails> findById(Long id);


}
