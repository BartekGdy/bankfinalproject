package bankApplication.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import sharedData.model.CustomerAppAddress;

import java.util.Optional;

@Repository
public interface AddressRepository extends JpaRepository<CustomerAppAddress,Long> {

    Optional<CustomerAppAddress> findById(Long id);
}
