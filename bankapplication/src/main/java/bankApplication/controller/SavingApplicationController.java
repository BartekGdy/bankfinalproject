package bankApplication.controller;

import bankApplication.model.service.SavingAccService;
import bankApplication.model.service.TokenAuthenticationService;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import sharedData.mapper.DtoMapper;
import sharedData.mapper.EntityMapper;
import sharedData.mapper.Mapper;
import sharedData.model.SavingApplication;
import sharedData.model.dto.SavingAppOutputDto;
import sharedData.model.dto.input.SavingApplicationDto;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping(value = "/saving-application")
public class SavingApplicationController {

    @Autowired
    private SavingAccService savingService;

    @Autowired
    @Qualifier("saving-input")
    private DtoMapper<SavingApplicationDto, SavingApplication> inputMapper;

    @Autowired
    @Qualifier("saving-output")
    private EntityMapper<SavingApplication, SavingAppOutputDto> outputMapper;
    @Autowired
    private TokenAuthenticationService tokenService;

    public SavingApplicationController() {
    }

    @RequestMapping(method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    @ApiOperation(value = "Current Account application", response = Long.class)
    @ApiResponse(code = 200, message = "ApplicationCustomer succesfull", response = Long.class)
    public ResponseEntity<Long> applyForCurrentAcc(@ApiParam("Current Acc ApplicationCustomer") @RequestBody SavingApplicationDto application, final HttpServletRequest request) {
        String userNameFromToken = tokenService.getUserNameFromToken(request);
        Long applicationId = savingService.applyForSavingAccount(userNameFromToken, inputMapper.mapDto(application));
        return new ResponseEntity<>(applicationId, HttpStatus.OK);
    }

    @RequestMapping(path = "/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiOperation(value = "View application", response = SavingAppOutputDto.class)
    @ApiResponses(value = {@ApiResponse(code = 200, message = "ApplicationCustomer details", response = SavingAppOutputDto.class),
            @ApiResponse(code = 404, message = "Appliation does not exist", response = Void.class)})
    public ResponseEntity<SavingAppOutputDto> getSavingApplication(@ApiParam("ApplicationCustomer id") @PathVariable("id") Long applicationId) {
        Optional<SavingApplication> applicationOptional = savingService.findApplication(applicationId);
        if (applicationOptional.isPresent()) {
            return new ResponseEntity<SavingAppOutputDto>(outputMapper.mapEntity(applicationOptional.get()), HttpStatus.OK);
        }
        return new ResponseEntity<SavingAppOutputDto>(HttpStatus.NOT_FOUND);
    }


    @RequestMapping(value = "/savingApplications", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiOperation(value = "View saving acc applications", response = List.class)
    @ApiResponses(value = {@ApiResponse(code = 200, message = "List of saving applications", response = List.class),
            @ApiResponse(code = 404, message = "Can't find specific user", response = Void.class)})
    public ResponseEntity<List<SavingAppOutputDto>> listSavingtApplication(final HttpServletRequest request) {
        String userNameFromToken = tokenService.getUserNameFromToken(request);

        List<SavingApplication> allSavingtAppsByCustomer = savingService.getAllSavingAppsByCustomer(userNameFromToken);
        List<SavingAppOutputDto> savingAppsDto = new ArrayList<>();
        for (SavingApplication savingApplication : allSavingtAppsByCustomer) {
            savingAppsDto.add(outputMapper.mapEntity(savingApplication));
        }
        return new ResponseEntity<List<SavingAppOutputDto>>(savingAppsDto, HttpStatus.OK);
    }

}
