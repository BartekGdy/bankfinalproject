package bankApplication.controller;

import bankApplication.model.service.CurrentAccService;
import bankApplication.model.service.TokenAuthenticationService;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import sharedData.mapper.DtoMapper;
import sharedData.mapper.EntityMapper;
import sharedData.model.CurrentApplication;
import sharedData.model.dto.CurrentAppOutputDto;
import sharedData.model.dto.input.CurrentApplicationDto;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping(value = "/current-application")
public class CurrentApplicationController {

    @Autowired
    private CurrentAccService currentAccService;
    @Autowired
    @Qualifier("current-input")
    private DtoMapper<CurrentApplicationDto,CurrentApplication> inputMapper;
    @Autowired
    @Qualifier("current-output")
    private EntityMapper<CurrentApplication, CurrentAppOutputDto> outputMapper;
    @Autowired
    private TokenAuthenticationService tokenService;

    @RequestMapping(method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    @ApiOperation(value = "Current Account application", response = Long.class)
    @ApiResponse(code = 200, message = "ApplicationCustomer succesfull", response = Long.class)
    public ResponseEntity<Long> applyForCurrentAcc(@ApiParam("Current Acc ApplicationCustomer") @RequestBody CurrentApplicationDto application, final HttpServletRequest request) {
        String userNameFromToken = tokenService.getUserNameFromToken(request);

        Long applicationId = currentAccService.applyForCurrentAccount(userNameFromToken, inputMapper.mapDto(application));
        return new ResponseEntity<>(applicationId, HttpStatus.OK);

    }

    @RequestMapping(path = "/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiOperation(value = "View application", response = CurrentAppOutputDto.class)
    @ApiResponses(value = {@ApiResponse(code = 200, message = "ApplicationCustomer details", response = CurrentAppOutputDto.class),
            @ApiResponse(code = 404, message = "ApplicationCustomer doesn not exist", response = Void.class)})
    public ResponseEntity<CurrentAppOutputDto> getCurrentApplication(@ApiParam("ApplicationCustomer id") @PathVariable("id") Long applicationId) {
        Optional<CurrentApplication> applicationOptional = currentAccService.findApplication(applicationId);
        if (applicationOptional.isPresent()) {
            return new ResponseEntity<CurrentAppOutputDto>(outputMapper.mapEntity(applicationOptional.get()), HttpStatus.OK);
        }
        return new ResponseEntity<CurrentAppOutputDto>(HttpStatus.NOT_FOUND);
    }

    @RequestMapping(path = "/currentApplications",method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiOperation(value = "View current acc applications", response = List.class)
    @ApiResponses(value = {@ApiResponse(code = 200, message = "List of current applications", response = List.class),
            @ApiResponse(code = 404, message = "Can't find specific user", response = Void.class)})
    public ResponseEntity<List<CurrentAppOutputDto>> listCurrentApplication(final HttpServletRequest request) {
        String userNameFromToken = tokenService.getUserNameFromToken(request);

        List<CurrentApplication> allCurrentAppsByCustomer = currentAccService.getAllCurrentAppsByCustomer(userNameFromToken);
        List<CurrentAppOutputDto> currentApplicationDto = new ArrayList<>();
        for (CurrentApplication application : allCurrentAppsByCustomer) {
            currentApplicationDto.add(outputMapper.mapEntity(application));
        }
        return new ResponseEntity<List<CurrentAppOutputDto>>(currentApplicationDto, HttpStatus.OK);
    }

}
