package bankApplication.controller;

import bankApplication.model.*;
import bankApplication.model.dto.input.CustomerDto;
import sharedData.model.dto.input.LoginDto;
import bankApplication.model.service.CustomerService;
import bankApplication.model.service.TokenAuthenticationService;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;
import sharedData.mapper.Mapper;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.Optional;

@RestController
@RequestMapping(value = "/customer")
public class CustomerController {

    @Autowired
    private CustomerService customerService;
    @Autowired
    private TokenAuthenticationService tokenService;
    @Autowired
    @Qualifier("customer")
    private Mapper<Customer, CustomerDto> customerMapper;

    @RequestMapping(method = RequestMethod.POST, produces = {
            MediaType.APPLICATION_JSON_VALUE}, consumes = MediaType.APPLICATION_JSON_VALUE)
    @ApiOperation(value = "Registration of customer", notes = "", response = Customer.class)
    @ApiResponses(value = {@ApiResponse(code = 200, message = "Customer registered", response = Customer.class),
            @ApiResponse(code = 400, message = "Incorrect input data", response = Void.class)})
    public ResponseEntity<Long> register(@ApiParam(value = "Registrated customer") @Valid @RequestBody CustomerDto customer) {
        Optional<Customer> createdCustomer = customerService.register(customerMapper.mapDto(customer));
        if (createdCustomer.isPresent()) {
            return new ResponseEntity<Long>(createdCustomer.get().getId(), HttpStatus.OK);
        }
        return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
    }

    @RequestMapping(value = "/login", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    @ApiOperation(value = "Logging customer.", notes = "", response = String.class)
    @ApiResponses(value = {@ApiResponse(code = 200, message = "Customer logged", response = String.class),
            @ApiResponse(code = 401, message = "Logging failed", response = Void.class)})
    public ResponseEntity<String> loginProcess(@ApiParam(value = "Login details") @RequestBody LoginDto login) {
        String token = tokenService
                .authenticate(new UsernamePasswordAuthenticationToken(login.getUsername(), login.getPassword()));
        if (StringUtils.isEmpty(token)) {
            return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
        }
        return new ResponseEntity<>(token, HttpStatus.OK);
    }
    @RequestMapping(method = RequestMethod.GET,produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiOperation(value = "View customer details", response = CustomerDto.class)
    @ApiResponses(value = {@ApiResponse(code = 200, message = "Customer details", response = CustomerDto.class),
            @ApiResponse(code = 404, message = "Can't find specific user", response = Void.class)})
    public ResponseEntity<CustomerDto> getCustomer(final HttpServletRequest request) {
        String userNameFromToken = tokenService.getUserNameFromToken(request);
        Optional<Customer> customer = customerService.findByUsername(userNameFromToken);
        if (customer.isPresent()){
            return new ResponseEntity<CustomerDto>(customerMapper.mapEntity(customer.get()),HttpStatus.OK);
        }
        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    @RequestMapping(path = "/{username}", method = RequestMethod.PUT, consumes = MediaType.APPLICATION_JSON_VALUE)
    @ApiOperation(value = "Customer eddition", response = Customer.class)
    @ApiResponses(value = {@ApiResponse(code = 200, message = "Details updated sucesfully", response = Customer.class),
            @ApiResponse(code = 404, message = "Can't find specific user", response = Void.class)})
    public ResponseEntity<CustomerDto> editCustomer(@Valid @ApiParam("New Details") @RequestBody CustomerDto customer) {
        Optional<Customer> customerOptional = customerService.editCustomerDetails(customer.getUsername(),customerMapper.mapDto(customer));
        if (customerOptional.isPresent()){
            return new ResponseEntity<CustomerDto>(customerMapper.mapEntity(customerOptional.get()),HttpStatus.OK);
        }
        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }


}
