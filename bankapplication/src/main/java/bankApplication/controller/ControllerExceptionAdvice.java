package bankApplication.controller;

import bankApplication.exception.AccountAlreadyCancellledException;
import bankApplication.exception.ApplicationNotFoundException;
import bankApplication.exception.CustomerNotFoundException;
import bankApplication.exception.UnauthorizedExcetion;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
public class ControllerExceptionAdvice {
	
	@ExceptionHandler
	public ResponseEntity<String> handleCustomerNotFoundException(CustomerNotFoundException e){
		return new ResponseEntity<String>("Customer not found",HttpStatus.NOT_FOUND);
	}

	@ExceptionHandler
	public ResponseEntity<String> handleAccAlreadyCancelledFoundException(AccountAlreadyCancellledException e){
		return new ResponseEntity<String>(e.getMessage(),HttpStatus.ALREADY_REPORTED);
	}

	@ExceptionHandler
	public ResponseEntity<String>handleUnauthorizedExcetion(UnauthorizedExcetion e){
		return new ResponseEntity<String>(e.getMessage(),HttpStatus.UNAUTHORIZED);
	}

	@ExceptionHandler
	public ResponseEntity<String> handleApplicationNotFoundException(ApplicationNotFoundException e){
		return new ResponseEntity<String>("Application not found",HttpStatus.NOT_FOUND);
	}
}
