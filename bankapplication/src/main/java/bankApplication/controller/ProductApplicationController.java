package bankApplication.controller;

import bankApplication.model.Customer;
import bankApplication.model.service.ProductAppService;
import bankApplication.model.service.TokenAuthenticationService;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import jdk.nashorn.internal.parser.Token;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import sharedData.mapper.DtoMapper;
import sharedData.mapper.EntityMapper;
import sharedData.mapper.Mapper;
import sharedData.model.ProductApp;
import sharedData.model.dto.ProductAppDto;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping(value = "/applications")
public class ProductApplicationController {

    @Autowired
    private ProductAppService productAppService;
    @Autowired
    @Qualifier("product")
    private EntityMapper<ProductApp, ProductAppDto> mapper;
    @Autowired
    private TokenAuthenticationService tokenService;


    @RequestMapping(method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiOperation(value = "View acc applications", response = List.class)
    @ApiResponses(value = {@ApiResponse(code = 200, message = "List of applications", response = List.class),
            @ApiResponse(code = 404, message = "Can't find specific user", response = Void.class)})
    public ResponseEntity<List<ProductAppDto>> listApplication(final HttpServletRequest request) {
        String userNameFromToken = tokenService.getUserNameFromToken(request);

        List<ProductApp> allApps = productAppService.getAllApplications(userNameFromToken);
        List<ProductAppDto> allAppsDto = new ArrayList<>();
        for (ProductApp app : allApps) {
            allAppsDto.add(mapper.mapEntity(app));
        }
        return new ResponseEntity<List<ProductAppDto>>(allAppsDto, HttpStatus.OK);
    }

    @RequestMapping(path = "/{id}/cancelApp", method = RequestMethod.PUT, consumes = MediaType.APPLICATION_JSON_VALUE)
    @ApiOperation(value = "Account application Cancellation", response = Long.class)
    @ApiResponses(value = {@ApiResponse(code = 200, message = "ApplicationCustomer cancelled", response = Long.class),
            @ApiResponse(code = 404, message = "Can't find specific user", response = Void.class)})
    public ResponseEntity<Long> cancelSavingAcc(@ApiParam("Acc ApplicationCustomer Number") @PathVariable("id") Long id,final HttpServletRequest request) {
        String userNameFromToken = tokenService.getUserNameFromToken(request);
        Long applicationId = productAppService.cancelApplication(id,userNameFromToken);
        return new ResponseEntity<>(applicationId, HttpStatus.OK);
    }

}
