package sharedData.model.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import sharedData.model.enums.EmploymentType;
import sharedData.model.enums.ProductType;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.time.LocalDate;

@ApiModel(description = "Current Account Application")
public class CurrentAppOutputDto implements Serializable{

    @ApiModelProperty("Id")
    @JsonProperty("id")
    private Long id;

    @ApiModelProperty(value = "Application Date")
    @JsonProperty("applicationDate")
    @JsonFormat(pattern = "dd/MM/yyyy")
    @NotNull
    private LocalDate apllicationDate;

    @ApiModelProperty("Owner")
    @JsonProperty("customer")
    private CustomerDetailsdDto customer;

    @ApiModelProperty(value = "Montyly Earnings")
    @JsonProperty("monthlyEarnings")
    @NotNull
    @Min(0)
    private Double monthlyEarnings;

    @ApiModelProperty(value = "Company Name")
    @JsonProperty("companyName")
    @NotNull
    @Size(min = 2,max = 100)
    private String companyName;

    @ApiModelProperty(value = "Employment Type")
    @JsonProperty("employmentType")
    @NotNull
    private EmploymentType employmentType;

    @ApiModelProperty(value = "Dependents")
    @JsonProperty("dependents")
    @NotNull
    @Min(0)
    private int nomberOfDependent;


    @ApiModelProperty(value = "Overdraft")
    @JsonProperty("overdraft")
    @NotNull
    @Min(0)
    private Double overdraftAmount;

    @ApiModelProperty(value = "Product Type")
    @JsonProperty("productType")
    @NotNull
    private ProductType productType;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public LocalDate getApllicationDate() {
        return apllicationDate;
    }

    public void setApllicationDate(LocalDate apllicationDate) {
        this.apllicationDate = apllicationDate;
    }

    public CustomerDetailsdDto getCustomer() {
        return customer;
    }

    public void setCustomer(CustomerDetailsdDto customer) {
        this.customer = customer;
    }

    public Double getMonthlyEarnings() {
        return monthlyEarnings;
    }

    public void setMonthlyEarnings(Double monthlyEarnings) {
        this.monthlyEarnings = monthlyEarnings;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public EmploymentType getEmploymentType() {
        return employmentType;
    }

    public void setEmploymentType(EmploymentType employmentType) {
        this.employmentType = employmentType;
    }

    public int getNomberOfDependent() {
        return nomberOfDependent;
    }

    public void setNomberOfDependent(int nomberOfDependent) {
        this.nomberOfDependent = nomberOfDependent;
    }

    public Double getOverdraftAmount() {
        return overdraftAmount;
    }

    public void setOverdraftAmount(Double overdraftAmount) {
        this.overdraftAmount = overdraftAmount;
    }

    public ProductType getProductType() {
        return productType;
    }

    public void setProductType(ProductType productType) {
        this.productType = productType;
    }
}
