package sharedData.model.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import sharedData.model.enums.AddressType;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import java.io.Serializable;

@ApiModel(description = "Customer Application Address")
public class CustAppAddressDto implements Serializable{

    @ApiModelProperty(value = "Premise")
    @JsonProperty("premise")
    private String premise;

    @ApiModelProperty(value = "Street")
    @JsonProperty("street")
    private String street;

    @ApiModelProperty(value = "Town")
    @JsonProperty("town")
    private String town;

    @ApiModelProperty(value = "PostCode")
    @JsonProperty("postCode")
    private String postCode;

    @ApiModelProperty(value = "Time at address")
    @JsonProperty("timeAtAddress")
    @Max(6)
    @Min(0)
    private int timeAtAddress;

    @ApiModelProperty(value = "CustomerAppAddress Type")
    @JsonProperty("addressType")
    private AddressType addressType;

    public CustAppAddressDto() {
    }

    public String getPremise() {
        return premise;
    }

    public void setPremise(String premise) {
        this.premise = premise;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getTown() {
        return town;
    }

    public void setTown(String town) {
        this.town = town;
    }

    public String getPostCode() {
        return postCode;
    }

    public void setPostCode(String postCode) {
        this.postCode = postCode;
    }

    public int getTimeAtAddress() {
        return timeAtAddress;
    }

    public void setTimeAtAddress(int timeAtAddress) {
        this.timeAtAddress = timeAtAddress;
    }

    public AddressType getAddressType() {
        return addressType;
    }

    public void setAddressType(AddressType addressType) {
        this.addressType = addressType;
    }
}

