package sharedData.model.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import sharedData.model.enums.ProductType;
import sharedData.model.enums.Status;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.time.LocalDate;

@ApiModel(description = "Application")
public class ProductAppDto implements Serializable{


    @ApiModelProperty("Id")
    @JsonProperty("id")
    private Long id;

    @ApiModelProperty(value = "Application Date")
    @JsonProperty("applicationDate")
    @JsonFormat(pattern = "dd/MM/yyyy")
    @NotNull
    private LocalDate apllicationDate;

    @ApiModelProperty(value = "Product")
    @JsonProperty("product")
    @NotNull
    private ProductType product;

    @ApiModelProperty(value = "Status")
    @JsonProperty("status")
    @NotNull
    private Status status;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public LocalDate getApllicationDate() {
        return apllicationDate;
    }

    public void setApllicationDate(LocalDate apllicationDate) {
        this.apllicationDate = apllicationDate;
    }

    public ProductType getProduct() {
        return product;
    }

    public void setProduct(ProductType product) {
        this.product = product;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }
}
