package sharedData.model.dto;

import sharedData.model.enums.Gender;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.time.LocalDate;
import java.util.List;

@ApiModel(value = "Eddited Customer")
public class CustomerDetailsdDto {


    @ApiModelProperty(value = "Name")
    @JsonProperty("firstname")
    private String firstname;
    @ApiModelProperty(value = "Surname")
    @JsonProperty("lastname")
    private String lastname;
    @ApiModelProperty(value = "DOB")
    @JsonProperty("DOB")
    private LocalDate dateOfBirth;
    @ApiModelProperty(value = "Gender")
    @JsonProperty("gender")
    private Gender gender;
    @ApiModelProperty(value = "Email")
    @JsonProperty("email")
    private String email;
    @ApiModelProperty(value = "CustomerAppAddress")
    @JsonProperty("addresses")
    private List<CustAppAddressDto> addresses;
    @ApiModelProperty(value = "Phone_Number")
    @JsonProperty("phone")
    private String phone;


    public List<CustAppAddressDto> getAddresses() {
        return addresses;
    }

    public void setAddresses(List<CustAppAddressDto> addresses) {
        this.addresses = addresses;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public LocalDate getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(LocalDate dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public Gender getGender() {
        return gender;
    }

    public void setGender(Gender gender) {
        this.gender = gender;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

}
