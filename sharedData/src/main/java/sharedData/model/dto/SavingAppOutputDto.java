package sharedData.model.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import sharedData.model.enums.ProductType;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.time.LocalDate;

@ApiModel(description = "Saving Account Application")
public class SavingAppOutputDto implements Serializable{

    @ApiModelProperty("Id")
    @JsonProperty("id")
    private Long id;

    @ApiModelProperty(value = "Application Date")
    @JsonProperty("applicationDate")
    @JsonFormat(pattern = "dd/MM/yyyy")
    @NotNull
    private LocalDate apllicationDate;

    @ApiModelProperty("Owner")
    @JsonProperty("customer")
    private CustomerDetailsdDto customer;

    @ApiModelProperty(value = "Montyly Earnings")
    @JsonProperty("monthlyEarnings")
    @NotNull
    @Min(0)
    private Double monthlyEarnings;

    @ApiModelProperty(value = "Product Type")
    @JsonProperty("productType")
    @NotNull
    private ProductType productType;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public LocalDate getApllicationDate() {
        return apllicationDate;
    }

    public void setApllicationDate(LocalDate apllicationDate) {
        this.apllicationDate = apllicationDate;
    }

    public CustomerDetailsdDto getCustomer() {
        return customer;
    }

    public void setCustomer(CustomerDetailsdDto customer) {
        this.customer = customer;
    }

    public Double getMonthlyEarnings() {
        return monthlyEarnings;
    }

    public void setMonthlyEarnings(Double monthlyEarnings) {
        this.monthlyEarnings = monthlyEarnings;
    }

    public ProductType getProductType() {
        return productType;
    }

    public void setProductType(ProductType productType) {
        this.productType = productType;
    }
}
