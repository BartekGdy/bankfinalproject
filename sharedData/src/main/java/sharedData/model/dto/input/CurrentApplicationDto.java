package sharedData.model.dto.input;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.hibernate.validator.constraints.NotEmpty;
import sharedData.model.enums.EmploymentType;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;

@ApiModel(description = "Current Account Application")
public class CurrentApplicationDto implements Serializable {



    @ApiModelProperty(value = "Montyly Earnings")
    @JsonProperty("monthlyEarnings")
    @NotNull
    @Min(0)
    private Double monthlyEarnings;

    @ApiModelProperty(value = "Company Name")
    @JsonProperty("companyName")
    @NotNull
    @Size(min = 2,max = 100)
    private String companyName;

    @ApiModelProperty(value = "Employment Type")
    @JsonProperty("employmentType")
    @NotNull
    private EmploymentType employmentType;

    @ApiModelProperty(value = "Dependents")
    @JsonProperty("dependents")
    @NotNull
    @Min(0)
    private int nomberOfDependent;

    @ApiModelProperty(value = "Overdraft")
    @JsonProperty("overdraft")
    @NotNull
    @Min(0)
    private Double overdraftAmount;

    public Double getMonthlyEarnings() {
        return monthlyEarnings;
    }

    public void setMonthlyEarnings(Double monthlyEarnings) {
        this.monthlyEarnings = monthlyEarnings;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public EmploymentType getEmploymentType() {
        return employmentType;
    }

    public void setEmploymentType(EmploymentType employmentType) {
        this.employmentType = employmentType;
    }

    public int getNomberOfDependent() {
        return nomberOfDependent;
    }

    public void setNomberOfDependent(int nomberOfDependent) {
        this.nomberOfDependent = nomberOfDependent;
    }

    public Double getOverdraftAmount() {
        return overdraftAmount;
    }

    public void setOverdraftAmount(Double overdraftAmount) {
        this.overdraftAmount = overdraftAmount;
    }
}
