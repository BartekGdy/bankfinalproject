package sharedData.model.dto.input;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

@ApiModel(description = "Current Account Application")
public class SavingApplicationDto implements Serializable{



    @ApiModelProperty(value = "Montyly Earnings")
    @JsonProperty("monthlyEarnings")
    @NotNull
    @Min(0)
    private Double monthlyEarnings;


    public Double getMonthlyEarnings() {
        return monthlyEarnings;
    }

    public void setMonthlyEarnings(Double monthlyEarnings) {
        this.monthlyEarnings = monthlyEarnings;
    }


}
