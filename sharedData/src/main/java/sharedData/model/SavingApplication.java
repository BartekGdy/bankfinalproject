package sharedData.model;

import sharedData.model.enums.ProductType;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

@Entity
public class SavingApplication extends ProductApp{

    @ManyToOne
    @NotNull
    @JoinColumn(name = "CUSTOMER_ID")
    private CustomerDetails customer;

    @NotNull
    @Min(0)
    private Double monthlyEarnings;


    public CustomerDetails getCustomer() {
        return customer;
    }

    public void setCustomer(CustomerDetails customer) {
        this.customer = customer;
    }

    public ProductType getProduct() {
        return super.getProduct();
    }

    public Double getMonthlyEarnings() {
        return monthlyEarnings;
    }

    public void setMonthlyEarnings(Double monthlyEarnings) {
        this.monthlyEarnings = monthlyEarnings;
    }

    public Long getId() {
        return super.getId();
    }

}
