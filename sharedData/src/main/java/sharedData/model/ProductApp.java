package sharedData.model;

import org.springframework.format.annotation.DateTimeFormat;
import sharedData.model.enums.ProductType;
import sharedData.model.enums.Status;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.Date;

@Entity
@Inheritance
public abstract class ProductApp {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @DateTimeFormat(pattern = "yyyy/MM/dd")
    @Temporal(TemporalType.DATE)
    @Column(name = "application_date")
    private Date apllicationDate = Timestamp.valueOf(LocalDateTime.now());

    @Enumerated(EnumType.STRING)
    @NotNull
    private ProductType product;
    @Enumerated(EnumType.STRING)
    @NotNull
    private Status status;

    public ProductApp() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getApllicationDate() {
        return apllicationDate;
    }

    public void setApllicationDate(Date apllicationDate) {
        this.apllicationDate = apllicationDate;
    }

    public ProductType getProduct() {
        return product;
    }

    public void setProduct(ProductType product) {
        this.product = product;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }
}
