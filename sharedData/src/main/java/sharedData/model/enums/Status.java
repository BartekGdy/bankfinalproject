package sharedData.model.enums;

public enum Status {
    ACTIVE,PENDING,ACCOUNT_OPENED,DECLINE,CANCELLED
}
