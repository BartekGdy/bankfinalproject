package sharedData.model;

import org.hibernate.validator.constraints.NotEmpty;
import sharedData.model.enums.EmploymentType;
import sharedData.model.enums.ProductType;

import javax.persistence.*;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Entity
public class CurrentApplication extends ProductApp{

    @ManyToOne
    @NotNull
    @JoinColumn(name = "CUSTOMER_ID")
    private CustomerDetails customer;

    @NotNull
    @Min(0)
    private Double monthlyEarnings;
    @NotEmpty
    @Size(min = 2, max = 100)
    private String companyName;
    @NotNull
    @Enumerated(EnumType.STRING)
    private EmploymentType employmentType;

    @NotNull
    @Min(0)
    @Column(name = "number_of_dependents")
    private int numberOfDependents;

    @NotNull
    @Min(0)
    private Double overdraftAmount;

    public ProductType getProduct() {
        return super.getProduct();
    }

    public CustomerDetails getCustomer() {
        return customer;
    }

    public void setCustomer(CustomerDetails customer) {
        this.customer = customer;
    }

    public Double getMonthlyEarnings() {
        return monthlyEarnings;
    }

    public void setMonthlyEarnings(Double monthlyEarnings) {
        this.monthlyEarnings = monthlyEarnings;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public EmploymentType getEmploymentType() {
        return employmentType;
    }

    public void setEmploymentType(EmploymentType employmentType) {
        this.employmentType = employmentType;
    }

    public int getNumberOfDependents() {
        return numberOfDependents;
    }

    public void setNumberOfDependents(int numberOfDependents) {
        this.numberOfDependents = numberOfDependents;
    }

    public Long getId() {
        return super.getId();
    }

    public Double getOverdraftAmount() {
        return overdraftAmount;
    }

    public void setOverdraftAmount(Double overdraftAmount) {
        this.overdraftAmount = overdraftAmount;
    }
}
