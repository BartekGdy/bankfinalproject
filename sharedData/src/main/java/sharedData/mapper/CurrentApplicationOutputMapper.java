package sharedData.mapper;

import sharedData.converter.LocalDateAttributeConverter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;
import sharedData.model.CurrentApplication;
import sharedData.model.CustomerDetails;
import sharedData.model.dto.CurrentAppOutputDto;
import sharedData.model.dto.CustomerDetailsdDto;

@Component("current-output")
public class CurrentApplicationOutputMapper implements EntityMapper<CurrentApplication, CurrentAppOutputDto> {


    @Autowired
    @Qualifier("customer-details")
    private Mapper<CustomerDetails, CustomerDetailsdDto> customerMapper;

    @Autowired
    private LocalDateAttributeConverter dateConverter;

    @Override
    public CurrentAppOutputDto mapEntity(CurrentApplication entity) {
        CurrentAppOutputDto dto = new CurrentAppOutputDto();
        dto.setId(entity.getId());
        dto.setApllicationDate(dateConverter.convert(entity.getApllicationDate()));
        dto.setCustomer(customerMapper.mapEntity(entity.getCustomer()));
        dto.setMonthlyEarnings(entity.getMonthlyEarnings());
        dto.setEmploymentType(entity.getEmploymentType());
        dto.setCompanyName(entity.getCompanyName());
        dto.setNomberOfDependent(entity.getNumberOfDependents());
        dto.setOverdraftAmount(entity.getOverdraftAmount());
        dto.setProductType(entity.getProduct());

        return dto;
    }

}
