package sharedData.mapper;

/**
 * Created by RENT on 2017-11-04.
 */
public interface DtoMapper<Dto, Entity> {
    Entity mapDto(Dto dto);
}
