package sharedData.mapper;

import sharedData.converter.LocalDateAttributeConverter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;
import sharedData.model.CustomerAppAddress;
import sharedData.model.CustomerDetails;
import sharedData.model.dto.CustAppAddressDto;
import sharedData.model.dto.CustomerDetailsdDto;

import java.util.ArrayList;
import java.util.List;

@Component("customer-details")
public class CustomerDetailsMapper implements Mapper<CustomerDetails, CustomerDetailsdDto> {

    @Autowired
    @Qualifier("address")
    private Mapper<CustomerAppAddress, CustAppAddressDto> mapperAddress;

    @Autowired
    LocalDateAttributeConverter dateConverter;

    @Override
    public CustomerDetailsdDto mapEntity(CustomerDetails entity) {
        CustomerDetailsdDto dto = new CustomerDetailsdDto();
        dto.setEmail(entity.getEmail());
        dto.setFirstname(entity.getFirstname());
        dto.setLastname(entity.getLastname());
        dto.setPhone(entity.getPhone());
        List<CustomerAppAddress> customerAppAddresses = entity.getCustomerAppAddresses();
        List<CustAppAddressDto> addressesDtos = new ArrayList<>();
        for (CustomerAppAddress customerAppAddress : customerAppAddresses) {
            addressesDtos.add(mapperAddress.mapEntity(customerAppAddress));
        }
        dto.setAddresses(addressesDtos);
        dto.setGender(entity.getGender());
        dto.setDateOfBirth(dateConverter.convert(entity.getDateOfBirth()));
        return dto;
    }

    @Override
    public CustomerDetails mapDto(CustomerDetailsdDto dto) {
        CustomerDetails entity = new CustomerDetails();
        entity.setEmail(dto.getEmail());
        entity.setFirstname(dto.getFirstname());
        entity.setLastname(dto.getLastname());
        entity.setPhone(dto.getPhone());
        List<CustAppAddressDto> addressesDto = dto.getAddresses();
        List<CustomerAppAddress> customerAppAddresses = new ArrayList<>();
        for (CustAppAddressDto custAppAddressDto : addressesDto) {
            customerAppAddresses.add(mapperAddress.mapDto(custAppAddressDto));
        }
        entity.setCustomerAppAddresses(customerAppAddresses);
        entity.setGender(dto.getGender());
        entity.setDateOfBirth(dateConverter.convert(dto.getDateOfBirth()));
        return entity;
    }

}
