package sharedData.mapper;

/**
 * Created by RENT on 2017-11-04.
 */
public interface EntityMapper<Entity, Dto> {
    Dto mapEntity(Entity entity);
}
