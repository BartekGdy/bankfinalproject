package sharedData.mapper;

import sharedData.converter.LocalDateAttributeConverter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;
import sharedData.model.CustomerDetails;
import sharedData.model.SavingApplication;
import sharedData.model.dto.CustomerDetailsdDto;
import sharedData.model.dto.SavingAppOutputDto;

@Component("saving-output")
public class SavingApplicationOutputMapper implements EntityMapper<SavingApplication, SavingAppOutputDto> {


    @Autowired
    @Qualifier("customer-details")
    private Mapper<CustomerDetails, CustomerDetailsdDto> customerMapper;

    @Autowired
    private LocalDateAttributeConverter dateConverter;

    @Override
    public SavingAppOutputDto mapEntity(SavingApplication entity) {
        SavingAppOutputDto dto = new SavingAppOutputDto();
        dto.setId(entity.getId());
        dto.setApllicationDate(dateConverter.convert(entity.getApllicationDate()));
        dto.setCustomer(customerMapper.mapEntity(entity.getCustomer()));
        dto.setMonthlyEarnings(entity.getMonthlyEarnings());
        dto.setProductType(entity.getProduct());
        return dto;
    }
}
