package sharedData.mapper;

import org.springframework.stereotype.Component;
import sharedData.model.CurrentApplication;
import sharedData.model.dto.input.CurrentApplicationDto;

@Component("current-input")
public class CurrentApplicationInputMapper implements DtoMapper<CurrentApplicationDto, CurrentApplication> {



    @Override
    public CurrentApplication mapDto(CurrentApplicationDto dto) {
        CurrentApplication entity = new CurrentApplication();
        entity.setCompanyName(dto.getCompanyName());
        entity.setNumberOfDependents(dto.getNomberOfDependent());
        entity.setEmploymentType(dto.getEmploymentType());
        entity.setMonthlyEarnings(dto.getMonthlyEarnings());
        entity.setOverdraftAmount(dto.getOverdraftAmount());
        return entity;
    }
}
