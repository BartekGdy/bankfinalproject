package sharedData.mapper;

import org.springframework.stereotype.Component;
import sharedData.model.SavingApplication;
import sharedData.model.dto.input.SavingApplicationDto;

@Component("saving-input")
public class SavingApplicationInputMapper implements DtoMapper<SavingApplicationDto,SavingApplication > {


    @Override
    public SavingApplication mapDto(SavingApplicationDto dto) {
        SavingApplication entity = new SavingApplication();
        entity.setMonthlyEarnings(dto.getMonthlyEarnings());
        return entity;
    }
}
