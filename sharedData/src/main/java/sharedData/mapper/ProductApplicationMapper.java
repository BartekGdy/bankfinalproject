package sharedData.mapper;

import sharedData.converter.LocalDateAttributeConverter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import sharedData.model.ProductApp;
import sharedData.model.dto.ProductAppDto;

@Component("product")
public class ProductApplicationMapper implements EntityMapper<ProductApp, ProductAppDto> {

    @Autowired
    private LocalDateAttributeConverter dateConverter;

    @Override
    public ProductAppDto mapEntity(ProductApp entity) {
        ProductAppDto dto = new ProductAppDto();
        dto.setId(entity.getId());
        dto.setProduct(entity.getProduct());
        dto.setStatus(entity.getStatus());
        dto.setApllicationDate(dateConverter.convert(entity.getApllicationDate()));
        return dto;
    }

}
