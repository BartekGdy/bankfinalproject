package sharedData.mapper;

import org.springframework.stereotype.Component;

@Component
public interface Mapper<Entity, Dto> extends EntityMapper<Entity, Dto>, DtoMapper<Dto, Entity> {

}
