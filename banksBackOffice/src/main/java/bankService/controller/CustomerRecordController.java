package bankService.controller;

import bankService.model.CustomerRecord;
import bankService.model.dto.CustomerRecordDto;
import bankService.model.dto.input.CustomerRecordInputDto;
import bankService.model.service.CustomerRecordService;
import bankService.model.service.TokenAuthenticationService;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import sharedData.converter.LocalDateAttributeConverter;
import sharedData.mapper.DtoMapper;
import sharedData.mapper.EntityMapper;
import sharedData.mapper.Mapper;
import sharedData.model.CurrentApplication;
import sharedData.model.dto.CurrentAppOutputDto;

import javax.validation.Valid;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping(value = "/customer-record")
public class CustomerRecordController {

    @Autowired
    private CustomerRecordService customerService;

    @Autowired
    @Qualifier("customer-input")
    private DtoMapper<CustomerRecordInputDto, CustomerRecord> inputMapper;

    @Autowired
    @Qualifier("customer-output")
    private EntityMapper<CustomerRecord, CustomerRecordDto> outputMapper;

    @Autowired
    private LocalDateAttributeConverter dateConverter;

    @RequestMapping(method = RequestMethod.POST, produces = {
            MediaType.APPLICATION_JSON_VALUE}, consumes = MediaType.APPLICATION_JSON_VALUE)
    @ApiOperation(value = "Creation of new customer", notes = "", response = CustomerRecordDto.class)
    @ApiResponses(value = {@ApiResponse(code = 200, message = "Customer created", response = CustomerRecordDto.class),
            @ApiResponse(code = 400, message = "Incorrect input data", response = Void.class)})
    public ResponseEntity<CustomerRecordDto> createNewCustomer(@ApiParam(value = "New Customer") @Valid @RequestBody CustomerRecordInputDto customer) {
        Optional<CustomerRecord> customerOptional = customerService.createCustomer(inputMapper.mapDto(customer));
        if (customerOptional.isPresent()) {
            return new ResponseEntity<>(outputMapper.mapEntity(customerOptional.get()), HttpStatus.OK);
        }
        return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
    }

    @RequestMapping(path = "/{id}", method = RequestMethod.PUT, consumes = MediaType.APPLICATION_JSON_VALUE)
    @ApiOperation(value = "Customer eddition", response = CustomerRecordDto.class)
    @ApiResponses(value = {@ApiResponse(code = 200, message = "Details updated sucesfully", response = CustomerRecordDto.class),
            @ApiResponse(code = 404, message = "Can't find specific user", response = Void.class)})
    public ResponseEntity<CustomerRecordDto> editCustomer(@ApiParam("Customer number") @PathVariable Long id, @Valid @ApiParam("New Details") @RequestBody CustomerRecordInputDto customer) {
        CustomerRecord customerRecord = customerService.editCustomerRecord(id, inputMapper.mapDto(customer));
        return new ResponseEntity<CustomerRecordDto>(outputMapper.mapEntity(customerRecord), HttpStatus.OK);
    }

    @RequestMapping(path = "/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiOperation(value = "View customer record", response = CustomerRecordDto.class)
    @ApiResponses(value = {@ApiResponse(code = 200, message = "Customer record details", response = CustomerRecordDto.class),
            @ApiResponse(code = 404, message = "Customer does not exist", response = Void.class)})
    public ResponseEntity<CustomerRecordDto> getCustomerRecord(@ApiParam("ApplicationCustomer id") @PathVariable("id") Long applicationId) {
        Optional<CustomerRecord> customerOptional = customerService.getCustomerById(applicationId);
        if (customerOptional.isPresent()) {
            return new ResponseEntity<CustomerRecordDto>(outputMapper.mapEntity(customerOptional.get()), HttpStatus.OK);
        }
        return new ResponseEntity<CustomerRecordDto>(HttpStatus.NOT_FOUND);
    }

    @RequestMapping(path = "/searchByLastNameAndPostCode/{lastName}/{postCode}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiOperation(value = "View customer's list", response = List.class)
    @ApiResponses(value = {@ApiResponse(code = 200, message = "Customer list details", response = List.class),
            @ApiResponse(code = 404, message = "No customers found", response = Void.class)})
    public ResponseEntity<List<CustomerRecordDto>> getCustomerRecordByLastNameAndPostCode(@ApiParam("Last Name") @PathVariable("lastName") String lastName,@ApiParam("Post Code") @PathVariable("postCode") String postCode ) {
        List<CustomerRecord> customersList = customerService.findCustomerByLastNameAndPostCode(lastName,postCode);
        if (customersList.isEmpty()) {
            return new ResponseEntity<List<CustomerRecordDto>>(HttpStatus.NOT_FOUND);
        }
        List<CustomerRecordDto> customerRecordDtos = new ArrayList<>();
        for (CustomerRecord customerRecord : customersList) {
            customerRecordDtos.add(outputMapper.mapEntity(customerRecord));
        }
        return new ResponseEntity<List<CustomerRecordDto>>(customerRecordDtos, HttpStatus.OK);
    }
    @RequestMapping(path = "searchByLastNameAndDob/{lastName}/", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiOperation(value = "View customer's list", response = List.class)
    @ApiResponses(value = {@ApiResponse(code = 200, message = "Customer list details", response = List.class),
            @ApiResponse(code = 404, message = "No customers found", response = Void.class)})
    public ResponseEntity<List<CustomerRecordDto>> getCustomerRecordByLastNameAndDob(@ApiParam("Last Name") @PathVariable("lastName") String lastName, @ApiParam("Date of Birth") @RequestParam("dob") @DateTimeFormat(pattern = "dd/MM/yyyy") LocalDate dob ) {
        List<CustomerRecord> customersList = customerService.findCustomerByLastNameAndDob(lastName,dateConverter.convert(dob));
        if (customersList.isEmpty()) {
            return new ResponseEntity<List<CustomerRecordDto>>(HttpStatus.NOT_FOUND);
        }
        List<CustomerRecordDto> customerRecordDtos = new ArrayList<>();
        for (CustomerRecord customerRecord : customersList) {
            customerRecordDtos.add(outputMapper.mapEntity(customerRecord));
        }
        return new ResponseEntity<List<CustomerRecordDto>>(customerRecordDtos, HttpStatus.OK);
    }
    @RequestMapping(path = "/searchByPremiseAndPostCode/{premise}/{postCode}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiOperation(value = "View customer's list", response = List.class)
    @ApiResponses(value = {@ApiResponse(code = 200, message = "Customer list details", response = List.class),
            @ApiResponse(code = 404, message = "No customers found", response = Void.class)})
    public ResponseEntity<List<CustomerRecordDto>> getCustomerRecordByPremiseAndPostCode(@ApiParam("Premise") @PathVariable("premise") String premise,@ApiParam("Post Code") @PathVariable("postCode") String postCode ) {
        List<CustomerRecord> customersList = customerService.findCustomerByPremiseAndPostCode(premise,postCode);
        if (customersList.isEmpty()) {
            return new ResponseEntity<List<CustomerRecordDto>>(HttpStatus.NOT_FOUND);
        }
        List<CustomerRecordDto> customerRecordDtos = new ArrayList<>();
        for (CustomerRecord customerRecord : customersList) {
            customerRecordDtos.add(outputMapper.mapEntity(customerRecord));
        }
        return new ResponseEntity<List<CustomerRecordDto>>(customerRecordDtos, HttpStatus.OK);
    }

    @RequestMapping(path = "/serachByProductNumber/{productNumber}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiOperation(value = "View customer's list", response = List.class)
    @ApiResponses(value = {@ApiResponse(code = 200, message = "Customer list details", response = List.class),
            @ApiResponse(code = 404, message = "No customers found", response = Void.class)})
    public ResponseEntity<List<CustomerRecordDto>> getCustomerRecordByProductNumber(@ApiParam("Product Number") @PathVariable("productNumber") String productNumber) {
        List<CustomerRecord> customersList = customerService.findCustomerByProductNumber(productNumber);
        if (customersList.isEmpty()) {
            return new ResponseEntity<List<CustomerRecordDto>>(HttpStatus.NOT_FOUND);
        }
        List<CustomerRecordDto> customerRecordDtos = new ArrayList<>();
        for (CustomerRecord customerRecord : customersList) {
            customerRecordDtos.add(outputMapper.mapEntity(customerRecord));
        }
        return new ResponseEntity<List<CustomerRecordDto>>(customerRecordDtos, HttpStatus.OK);
    }
}
