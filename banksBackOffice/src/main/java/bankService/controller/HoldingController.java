package bankService.controller;

import bankService.exception.CustomerNotFoundException;
import bankService.model.CustomerRecord;
import bankService.model.Holding;
import bankService.model.ProcessedCurrentApplication;
import bankService.model.dto.HoldingDto;
import bankService.model.dto.ProcessedCurrentApplicaitonOutputDto;
import bankService.model.service.CustomerRecordService;
import bankService.model.service.HoldingService;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import sharedData.mapper.EntityMapper;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping(value = "/holding")
public class HoldingController {

    @Autowired
    private HoldingService holdingService;
    @Autowired
    private CustomerRecordService recordService;
    @Autowired
    @Qualifier("holding")
    private EntityMapper<Holding,HoldingDto> mapper;

    @RequestMapping(path = "/{customerID}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiOperation(value = "View holdings", response = List.class)
    @ApiResponses(value = {@ApiResponse(code = 200, message = "Holding details", response = List.class),
            @ApiResponse(code = 404, message = "No holdings found", response = Void.class)})
    public ResponseEntity<List<HoldingDto>> getHoldingList(@ApiParam("customer id") @PathVariable("customerID") Long caseId) {
        Optional<CustomerRecord> customerOptional = recordService.getCustomerById(caseId);
        if (!customerOptional.isPresent()){
            throw new CustomerNotFoundException();
        }
        CustomerRecord customerRecord = customerOptional.get();
        List<Holding> holdings = holdingService.getAllHoldingsForCustomer(customerRecord);
        if (holdings.isEmpty()){
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        List<HoldingDto> holdingsDto = new ArrayList<>();
        for (Holding holding : holdings) {
            holdingsDto.add(mapper.mapEntity(holding));
        }
        return new ResponseEntity<List<HoldingDto>>(holdingsDto,HttpStatus.OK);
    }
}
