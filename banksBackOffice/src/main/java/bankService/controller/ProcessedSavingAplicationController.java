package bankService.controller;

import bankService.exception.AdvisorNotFoundException;
import bankService.model.AdvisorEntity;
import bankService.model.CustomerRecord;
import bankService.model.ProcessedCurrentApplication;
import bankService.model.ProcessedSavingApplication;
import bankService.model.dto.CustomerRecordDto;
import bankService.model.dto.ProcessedCurrentApplicaitonOutputDto;
import bankService.model.dto.ProcessedSavingApplicaitonOutputDto;
import bankService.model.dto.input.CustomerRecordInputDto;
import bankService.model.dto.input.ProcessedSavingApplicaitonInputDto;
import bankService.model.service.*;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import sharedData.mapper.DtoMapper;
import sharedData.mapper.EntityMapper;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.Optional;

@RestController
@RequestMapping(value = "/savCase")
public class ProcessedSavingAplicationController {

    @Autowired
    private ProcessedSavingAppService caseService;
    @Autowired
    private AdvisorService advisorService;
    @Autowired
    private TokenAuthenticationService tokenService;

    @Autowired
    @Qualifier("sav-case-input")
    private DtoMapper<ProcessedSavingApplicaitonInputDto, ProcessedSavingApplication> inputMapper;
    @Autowired
    @Qualifier("sav-case-output")
    private EntityMapper<ProcessedSavingApplication,ProcessedSavingApplicaitonOutputDto> outputMapper;


    @RequestMapping(method = RequestMethod.POST, produces = {
            MediaType.APPLICATION_JSON_VALUE}, consumes = MediaType.APPLICATION_JSON_VALUE)
    @ApiOperation(value = "Creating new case", notes = "", response = ProcessedSavingApplicaitonOutputDto.class)
    @ApiResponses(value = {@ApiResponse(code = 200, message = "Case created", response = ProcessedSavingApplicaitonOutputDto.class),
            @ApiResponse(code = 400, message = "Incorrect input data", response = Void.class)})
    public ResponseEntity<ProcessedSavingApplicaitonOutputDto> createCase(@ApiParam(value = "Application") @Valid @RequestBody ProcessedSavingApplicaitonInputDto appCase, final HttpServletRequest request) {
        ProcessedSavingApplication processedApp = inputMapper.mapDto(appCase);
        String userNameFromToken = tokenService.getUserNameFromToken(request);
        Optional<AdvisorEntity> advisoryOptional = advisorService.findByUsername(userNameFromToken);
        if (!advisoryOptional.isPresent()){
            throw new AdvisorNotFoundException();
        }
        processedApp.setAdvisor(advisoryOptional.get());
        Optional<ProcessedSavingApplication> caseOptional = caseService.createNewCase(processedApp);
        if (caseOptional.isPresent()) {
            return new ResponseEntity<ProcessedSavingApplicaitonOutputDto>(outputMapper.mapEntity(caseOptional.get()), HttpStatus.OK);
        }
        return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
    }

    @RequestMapping(path = "/findCase/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiOperation(value = "View case", response = ProcessedSavingApplicaitonOutputDto.class)
    @ApiResponses(value = {@ApiResponse(code = 200, message = "Case details", response = ProcessedSavingApplicaitonOutputDto.class),
            @ApiResponse(code = 404, message = "Application  does not exist", response = Void.class)})
    public ResponseEntity<ProcessedSavingApplicaitonOutputDto> getCase(@ApiParam("Case id") @PathVariable("id") Long caseId) {
        Optional<ProcessedSavingApplication> caseOptional = caseService.findById(caseId);
        if (caseOptional.isPresent()) {
            return new ResponseEntity<ProcessedSavingApplicaitonOutputDto>(outputMapper.mapEntity(caseOptional.get()), HttpStatus.OK);
        }
        return new ResponseEntity<ProcessedSavingApplicaitonOutputDto>(HttpStatus.NOT_FOUND);
    }

    @RequestMapping(path = "/exitCase/{id}", method = RequestMethod.PUT, consumes = MediaType.APPLICATION_JSON_VALUE)
    @ApiOperation(value = "Exiting case", response = ProcessedSavingApplicaitonOutputDto.class)
    @ApiResponse(code = 200, message = "Advisor exitted case", response = ProcessedSavingApplicaitonOutputDto.class)
    public ResponseEntity<ProcessedSavingApplicaitonOutputDto> exitCase(@ApiParam("Case number") @PathVariable Long id, final HttpServletRequest request) {
        String userNameFromToken = tokenService.getUserNameFromToken(request);
        ProcessedSavingApplication processedCase = caseService.exitCase(id, userNameFromToken);
        return new ResponseEntity<ProcessedSavingApplicaitonOutputDto>(outputMapper.mapEntity(processedCase),HttpStatus.OK);
    }

    @RequestMapping(path = "/{id}", method = RequestMethod.PUT, consumes = MediaType.APPLICATION_JSON_VALUE)
    @ApiOperation(value = "Assigning case", response = ProcessedSavingApplicaitonOutputDto.class)
    @ApiResponse(code = 200, message = "Advisor assigned case", response = ProcessedSavingApplicaitonOutputDto.class)
    public ResponseEntity<ProcessedSavingApplicaitonOutputDto> assignCase(@ApiParam("Case number") @PathVariable Long id, final HttpServletRequest request) {
        String userNameFromToken = tokenService.getUserNameFromToken(request);
        ProcessedSavingApplication processedCase = caseService.assignCase(id, userNameFromToken);
        return new ResponseEntity<ProcessedSavingApplicaitonOutputDto>(outputMapper.mapEntity(processedCase),HttpStatus.OK);
    }

    @RequestMapping(path = "/processCase/{id}", method = RequestMethod.PUT, consumes = MediaType.APPLICATION_JSON_VALUE)
    @ApiOperation(value = "Processing Case", response = ProcessedSavingApplicaitonOutputDto.class)
    @ApiResponses(value = {@ApiResponse(code = 200, message = "Case processed", response = ProcessedSavingApplicaitonOutputDto.class),
            @ApiResponse(code = 404, message = "Application does not exist", response = Void.class)})
    public ResponseEntity<ProcessedSavingApplicaitonOutputDto> processCase(@ApiParam("Case number") @PathVariable Long id, final HttpServletRequest request) {
        String userNameFromToken = tokenService.getUserNameFromToken(request);
        Optional<ProcessedSavingApplication> caseOptional = caseService.findById(id);
        if (caseOptional.isPresent()){
            ProcessedSavingApplication processeCase = caseOptional.get();
            ProcessedSavingApplication processedSavingApplication = caseService.processCase(userNameFromToken, processeCase);
            return new ResponseEntity<ProcessedSavingApplicaitonOutputDto>(outputMapper.mapEntity(processedSavingApplication),HttpStatus.OK);
        }
        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    @RequestMapping(path = "/cancelCase/{id}", method = RequestMethod.PUT, consumes = MediaType.APPLICATION_JSON_VALUE)
    @ApiOperation(value = "Cancelling case", response = ProcessedSavingApplicaitonOutputDto.class)
    @ApiResponse(code = 200, message = "Case cancelled", response = ProcessedSavingApplicaitonOutputDto.class)
    public ResponseEntity<ProcessedSavingApplicaitonOutputDto> cancellCase(@ApiParam("Case number") @PathVariable Long id, final HttpServletRequest request) {
        String userNameFromToken = tokenService.getUserNameFromToken(request);
        ProcessedSavingApplication processedCase = caseService.cancelCase(id, userNameFromToken);
        return new ResponseEntity<ProcessedSavingApplicaitonOutputDto>(outputMapper.mapEntity(processedCase),HttpStatus.OK);
    }
}
