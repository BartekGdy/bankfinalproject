package bankService.controller;

import bankService.model.service.CurrentAppService;
import bankService.model.service.TokenAuthenticationService;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import sharedData.mapper.EntityMapper;
import sharedData.model.CurrentApplication;
import sharedData.model.dto.CurrentAppOutputDto;
import sharedData.model.dto.input.CurrentApplicationDto;
import sharedData.model.enums.Status;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping(value = "/current-application")
public class CurrentApplicationController {

    @Autowired
    private CurrentAppService currentAppService;
    @Autowired
    @Qualifier("current-output")
    private EntityMapper<CurrentApplication, CurrentAppOutputDto> outputMapper;



    @RequestMapping(path = "/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiOperation(value = "View application", response = CurrentAppOutputDto.class)
    @ApiResponses(value = {@ApiResponse(code = 200, message = "Application details", response = CurrentAppOutputDto.class),
            @ApiResponse(code = 404, message = "Application  does not exist", response = Void.class)})
    public ResponseEntity<CurrentAppOutputDto> getCurrentApplication(@ApiParam("Application id") @PathVariable("id") Long applicationId) {
        Optional<CurrentApplication> applicationOptional = currentAppService.findApplication(applicationId);
        if (applicationOptional.isPresent()) {
            return new ResponseEntity<CurrentAppOutputDto>(outputMapper.mapEntity(applicationOptional.get()), HttpStatus.OK);
        }
        return new ResponseEntity<CurrentAppOutputDto>(HttpStatus.NOT_FOUND);
    }

    @RequestMapping(path = "/getCurrentApps/{status}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiOperation(value = "View application", response = CurrentApplicationDto.class)
    @ApiResponses(value = {@ApiResponse(code = 200, message = "Application details", response = CurrentApplicationDto.class),
            @ApiResponse(code = 404, message = "Appliation does not exist", response = Void.class)})
    public ResponseEntity<List<CurrentAppOutputDto>> getSavingApplicationsByStatus(@ApiParam("status") @PathVariable("status") Status status) {
        List<CurrentApplication> listApps = currentAppService.getCurrentsAppsWithStatus(status);
        List<CurrentAppOutputDto> currentApps = new ArrayList<>();
        for (CurrentApplication currentApp : listApps) {
            currentApps.add(outputMapper.mapEntity(currentApp));
        }
        return new ResponseEntity<List<CurrentAppOutputDto>>(currentApps, HttpStatus.OK);
    }

    @RequestMapping(path = "/getNewCurrentApp/{status}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiOperation(value = "Get current application", response = CurrentApplicationDto.class)
    @ApiResponses(value = {@ApiResponse(code = 200, message = "Application details", response = CurrentApplicationDto.class),
            @ApiResponse(code = 404, message = "Appliation does not exist", response = Void.class)})
    public ResponseEntity<CurrentAppOutputDto> getNextCurrentApplication(@ApiParam("status") @PathVariable("status") Status status) {
        Optional<CurrentApplication> applicationOptional = currentAppService.getNextCurrentAppWithStatus(status);
        if (applicationOptional.isPresent()) {
            CurrentAppOutputDto currentAppOutputDto = outputMapper.mapEntity(applicationOptional.get());
            return new ResponseEntity<CurrentAppOutputDto>(currentAppOutputDto, HttpStatus.OK);
        }
        return new ResponseEntity<CurrentAppOutputDto>(HttpStatus.NOT_FOUND);
    }

}
