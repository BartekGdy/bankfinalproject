package bankService.controller;

import bankService.exception.AdvisorNotFoundException;
import bankService.exception.UnauthorizedExcetion;
import bankService.model.AdvisorEntity;
import bankService.model.ProcessedCurrentApplication;
import bankService.model.ProcessedSavingApplication;
import bankService.model.dto.ProcessedCurrentApplicaitonOutputDto;
import bankService.model.dto.ProcessedSavingApplicaitonOutputDto;
import bankService.model.dto.input.ProcessedCurrentApplicaitonInputDto;
import bankService.model.service.*;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import sharedData.mapper.DtoMapper;
import sharedData.mapper.EntityMapper;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.Optional;

@RestController
@RequestMapping(value = "/currCase")
public class ProcessedCurrApplicationController {

    @Autowired
    private ProcessedCurrAppService caseService;
    @Autowired
    private AdvisorService advisorService;
    @Autowired
    private TokenAuthenticationService tokenService;

    @Autowired
    @Qualifier("cur-case-input")
    private DtoMapper<ProcessedCurrentApplicaitonInputDto, ProcessedCurrentApplication> inputMapper;
    @Autowired
    @Qualifier("cur-case-output")
    private EntityMapper<ProcessedCurrentApplication,ProcessedCurrentApplicaitonOutputDto> outputMapper;


    @RequestMapping(method = RequestMethod.POST, produces = {
            MediaType.APPLICATION_JSON_VALUE}, consumes = MediaType.APPLICATION_JSON_VALUE)
    @ApiOperation(value = "Craating new case", notes = "", response = ProcessedCurrentApplicaitonOutputDto.class)
    @ApiResponses(value = {@ApiResponse(code = 200, message = "Customer registered", response = ProcessedCurrentApplicaitonOutputDto.class),
            @ApiResponse(code = 400, message = "Incorrect input data", response = Void.class)})
    public ResponseEntity<ProcessedCurrentApplicaitonOutputDto> craeteCase(@ApiParam(value = "Registrated customer") @Valid @RequestBody ProcessedCurrentApplicaitonInputDto appCase, final HttpServletRequest request) {
        ProcessedCurrentApplication processedApp = inputMapper.mapDto(appCase);
        String userNameFromToken = tokenService.getUserNameFromToken(request);
        Optional<AdvisorEntity> advisoryOptional = advisorService.findByUsername(userNameFromToken);
        if (!advisoryOptional.isPresent()){
            throw new AdvisorNotFoundException();
        }
        processedApp.setAdvisor(advisoryOptional.get());
        Optional<ProcessedCurrentApplication> caseOptional = caseService.createNewCase(processedApp);
        if (caseOptional.isPresent()) {
            return new ResponseEntity<ProcessedCurrentApplicaitonOutputDto>(outputMapper.mapEntity(caseOptional.get()), HttpStatus.OK);
        }
        return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
    }

    @RequestMapping(path = "/findCase/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiOperation(value = "View case", response = ProcessedCurrentApplicaitonOutputDto.class)
    @ApiResponses(value = {@ApiResponse(code = 200, message = "Case details", response = ProcessedCurrentApplicaitonOutputDto.class),
            @ApiResponse(code = 404, message = "Application  does not exist", response = Void.class)})
    public ResponseEntity<ProcessedCurrentApplicaitonOutputDto> getCase(@ApiParam("Case id") @PathVariable("id") Long caseId) {
        Optional<ProcessedCurrentApplication> caseOptional = caseService.findById(caseId);
        if (caseOptional.isPresent()) {
            return new ResponseEntity<ProcessedCurrentApplicaitonOutputDto>(outputMapper.mapEntity(caseOptional.get()), HttpStatus.OK);
        }
        return new ResponseEntity<ProcessedCurrentApplicaitonOutputDto>(HttpStatus.NOT_FOUND);
    }
    @RequestMapping(path = "/exitCase/{id}", method = RequestMethod.PUT, consumes = MediaType.APPLICATION_JSON_VALUE)
    @ApiOperation(value = "Exiting case", response = ProcessedCurrentApplicaitonOutputDto.class)
    @ApiResponse(code = 200, message = "Advisor exitted case", response = ProcessedCurrentApplicaitonOutputDto.class)
    public ResponseEntity<ProcessedCurrentApplicaitonOutputDto> exitCase(@ApiParam("Case number") @PathVariable Long id, final HttpServletRequest request) {
        String userNameFromToken = tokenService.getUserNameFromToken(request);
        ProcessedCurrentApplication processedCase = caseService.exitCase(id, userNameFromToken);
        return new ResponseEntity<ProcessedCurrentApplicaitonOutputDto>(outputMapper.mapEntity(processedCase),HttpStatus.OK);
    }

    @RequestMapping(path = "/{id}", method = RequestMethod.PUT, consumes = MediaType.APPLICATION_JSON_VALUE)
    @ApiOperation(value = "Assigning case", response = ProcessedCurrentApplicaitonOutputDto.class)
    @ApiResponse(code = 200, message = "Advisor assigned case", response = ProcessedCurrentApplicaitonOutputDto.class)
    public ResponseEntity<ProcessedCurrentApplicaitonOutputDto> assignCase(@ApiParam("Case number") @PathVariable Long id, final HttpServletRequest request) {
        String userNameFromToken = tokenService.getUserNameFromToken(request);
        ProcessedCurrentApplication processedCase = caseService.assignCase(id, userNameFromToken);
        return new ResponseEntity<ProcessedCurrentApplicaitonOutputDto>(outputMapper.mapEntity(processedCase),HttpStatus.OK);
    }

    @RequestMapping(path = "/processCase/{id}", method = RequestMethod.PUT, consumes = MediaType.APPLICATION_JSON_VALUE)
    @ApiOperation(value = "Processing Case", response = ProcessedCurrentApplicaitonOutputDto.class)
    @ApiResponses(value = {@ApiResponse(code = 200, message = "Case processed", response = ProcessedCurrentApplicaitonOutputDto.class),
            @ApiResponse(code = 404, message = "Application does not exist", response = Void.class)})
    public ResponseEntity<ProcessedCurrentApplicaitonOutputDto> processCase(@ApiParam("Case number") @PathVariable Long id, final HttpServletRequest request) {
        String userNameFromToken = tokenService.getUserNameFromToken(request);
        Optional<ProcessedCurrentApplication> caseOptional = caseService.findById(id);
        if (caseOptional.isPresent()){
            ProcessedCurrentApplication processeCase = caseOptional.get();
            ProcessedCurrentApplication processedCurrentApplication = caseService.processCase(userNameFromToken, processeCase);
            return new ResponseEntity<ProcessedCurrentApplicaitonOutputDto>(outputMapper.mapEntity(processedCurrentApplication),HttpStatus.OK);
        }
        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    @RequestMapping(path = "/cancelCase/{id}", method = RequestMethod.PUT, consumes = MediaType.APPLICATION_JSON_VALUE)
    @ApiOperation(value = "Cancelling case", response = ProcessedCurrentApplicaitonOutputDto.class)
    @ApiResponse(code = 200, message = "Case cancelled", response = ProcessedCurrentApplicaitonOutputDto.class)
    public ResponseEntity<ProcessedCurrentApplicaitonOutputDto> cancellCase(@ApiParam("Case number") @PathVariable Long id, final HttpServletRequest request) {
        String userNameFromToken = tokenService.getUserNameFromToken(request);
        ProcessedCurrentApplication processedCase = caseService.cancelCase(id, userNameFromToken);
        return new ResponseEntity<ProcessedCurrentApplicaitonOutputDto>(outputMapper.mapEntity(processedCase),HttpStatus.OK);
    }

}
