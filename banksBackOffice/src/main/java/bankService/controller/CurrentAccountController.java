package bankService.controller;

import bankService.model.CurrentAccount;
import bankService.model.dto.CurrentAccountDto;
import bankService.model.service.CurrentAccountService;
import bankService.model.service.CurrentAppService;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import sharedData.mapper.EntityMapper;
import sharedData.model.CurrentApplication;
import sharedData.model.dto.CurrentAppOutputDto;

import java.util.Optional;

@RestController
@RequestMapping(value = "/currentAcc")
public class CurrentAccountController {

    @Autowired
    private CurrentAccountService accountService;

    @Autowired
    @Qualifier("curr-acc")
    private EntityMapper<CurrentAccount,CurrentAccountDto> mapper;

    @RequestMapping(path = "/{accNumber}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiOperation(value = "View account details", response = CurrentAccountDto.class)
    @ApiResponses(value = {@ApiResponse(code = 200, message = "Account details", response = CurrentAccountDto.class),
            @ApiResponse(code = 404, message = "Account does not exist", response = Void.class)})
    public ResponseEntity<CurrentAccountDto> getCurrentAccount(@ApiParam("Account number") @PathVariable("accNumber") String accNumber) {
        Optional<CurrentAccount> accountOptional = accountService.findByAccNumber(accNumber);
        if (accountOptional.isPresent()) {
            return new ResponseEntity<CurrentAccountDto>(mapper.mapEntity(accountOptional.get()), HttpStatus.OK);
        }
        return new ResponseEntity<CurrentAccountDto>(HttpStatus.NOT_FOUND);
    }
}
