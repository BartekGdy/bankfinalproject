package bankService.controller;

import bankService.model.AdvisorEntity;
import bankService.model.dto.input.AdvisorInputDto;
import bankService.model.dto.AdvisorOutputDto;
import bankService.model.service.AdvisorService;
import bankService.model.service.TokenAuthenticationService;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import sharedData.mapper.DtoMapper;
import sharedData.mapper.EntityMapper;
import sharedData.model.dto.input.LoginDto;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.Optional;

@RestController
@RequestMapping(value = "/advisor")
public class AdvisorController {

    @Autowired
    private AdvisorService advisorService;

    @Autowired
    private TokenAuthenticationService tokenService;

    @Autowired
    @Qualifier("advisor-input")
    private DtoMapper<AdvisorInputDto,AdvisorEntity> inputMapper;
    @Autowired
    @Qualifier("advisor-output")
    private EntityMapper<AdvisorEntity,AdvisorOutputDto> outputMapper;

    @RequestMapping(method = RequestMethod.POST, produces = {
            MediaType.APPLICATION_JSON_VALUE}, consumes = MediaType.APPLICATION_JSON_VALUE)
    @ApiOperation(value = "Registration of customer", notes = "", response = Long.class)
    @ApiResponses(value = {@ApiResponse(code = 200, message = "Customer registered", response = Long.class),
            @ApiResponse(code = 400, message = "Incorrect input data", response = Void.class)})
    public ResponseEntity<AdvisorOutputDto> register(@ApiParam(value = "Registrated customer") @Valid @RequestBody AdvisorInputDto customer) {
        Optional<AdvisorEntity> advisorOptional = advisorService.createAdvisor(inputMapper.mapDto(customer));
        if (advisorOptional.isPresent()) {
            return new ResponseEntity<AdvisorOutputDto>(outputMapper.mapEntity(advisorOptional.get()), HttpStatus.OK);
        }
        return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
    }

    @RequestMapping(value = "/login", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    @ApiOperation(value = "Logging advisor.", notes = "", response = String.class)
    @ApiResponses(value = {@ApiResponse(code = 200, message = "Advisor logged", response = String.class),
            @ApiResponse(code = 401, message = "Logging failed", response = Void.class)})
    public ResponseEntity<String> loginProcess(@ApiParam(value = "Login details") @RequestBody LoginDto login) {
        String token = tokenService
                .authenticate(new UsernamePasswordAuthenticationToken(login.getUsername(), login.getPassword()));
        if (StringUtils.isEmpty(token)) {
            return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
        }
        return new ResponseEntity<>(token, HttpStatus.OK);
    }

}
