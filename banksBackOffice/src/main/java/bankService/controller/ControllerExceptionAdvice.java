package bankService.controller;


import bankService.exception.*;
import io.swagger.annotations.ApiResponse;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
public class ControllerExceptionAdvice {
	
	@ExceptionHandler
	@ApiResponse(code = 404, message = "Customer Record not found", response = Void.class)
	public ResponseEntity<String> handleCustomerNotFound(CustomerNotFoundException e){
		return new ResponseEntity<String>("Customer Record not found",HttpStatus.NOT_FOUND);
	}
	@ExceptionHandler
	@ApiResponse(code = 404, message = "Advisor not found", response = Void.class)
	public ResponseEntity<String> handleAdvisorNotFound(AdvisorNotFoundException e){
		return new ResponseEntity<String>("Advisor not found",HttpStatus.NOT_FOUND);
	}

	@ExceptionHandler
	@ApiResponse(code = 404, message = "Application not found", response = Void.class)
	public ResponseEntity<String> handleApplicationNotFound(ApplicationNotFoundException e){
		return new ResponseEntity<String>("Application not found",HttpStatus.NOT_FOUND);
	}


	@ExceptionHandler
	@ApiResponse(code = 404, message = "Case not found", response = Void.class)
	public ResponseEntity<String> handlCaseNotFoundException(CaseNotFoundException e){
		return new ResponseEntity<String>("Case not found",HttpStatus.NOT_FOUND);
	}
	@ExceptionHandler
	public ResponseEntity<String>handleUnauthorizedExcetion(UnauthorizedExcetion e){
		return new ResponseEntity<String>(e.getMessage(),HttpStatus.UNAUTHORIZED);
	}

	@ExceptionHandler
	public ResponseEntity<String>handleCaseBlockedException(CaseBlockedException e){
		return new ResponseEntity<String>(e.getMessage(),HttpStatus.FORBIDDEN);
	}
}
