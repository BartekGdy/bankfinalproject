package bankService.controller;

import bankService.model.CurrentAccount;
import bankService.model.SavingAccount;
import bankService.model.dto.CurrentAccountDto;
import bankService.model.dto.SavingAccountDto;
import bankService.model.service.CurrentAccountService;
import bankService.model.service.SavingAccountService;
import bankService.model.service.SavingAppService;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import sharedData.mapper.EntityMapper;

import java.util.Optional;

@RestController
@RequestMapping(value = "/savingAcc")
public class SavingAccountController {

    @Autowired
    private SavingAccountService accountService;

    @Autowired
    @Qualifier("sav-acc")
    private EntityMapper<SavingAccount,SavingAccountDto> mapper;

    @RequestMapping(path = "/{accNumber}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiOperation(value = "View account details", response = SavingAccountDto.class)
    @ApiResponses(value = {@ApiResponse(code = 200, message = "Account details", response = SavingAccountDto.class),
            @ApiResponse(code = 404, message = "Account does not exist", response = Void.class)})
    public ResponseEntity<SavingAccountDto> getSavingAccount(@ApiParam("Account number") @PathVariable("accNumber") String accNumber) {
        Optional<SavingAccount> accountOptional = accountService.findByAccNumber(accNumber);
        if (accountOptional.isPresent()) {
            return new ResponseEntity<SavingAccountDto>(mapper.mapEntity(accountOptional.get()), HttpStatus.OK);
        }
        return new ResponseEntity<SavingAccountDto>(HttpStatus.NOT_FOUND);
    }
}
