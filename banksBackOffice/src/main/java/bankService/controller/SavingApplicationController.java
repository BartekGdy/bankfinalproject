package bankService.controller;


import bankService.model.service.SavingAppService;
import bankService.model.service.TokenAuthenticationService;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import sharedData.mapper.EntityMapper;
import sharedData.model.SavingApplication;
import sharedData.model.dto.SavingAppOutputDto;
import sharedData.model.dto.input.CurrentApplicationDto;
import sharedData.model.enums.Status;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping(value = "/saving-application")
public class SavingApplicationController {

    @Autowired
    private SavingAppService savingService;

    @Autowired
    @Qualifier("saving-output")
    private EntityMapper<SavingApplication, SavingAppOutputDto> outputMapper;
    @Autowired
    private TokenAuthenticationService tokenService;

    public SavingApplicationController() {
    }

    @RequestMapping(path = "/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiOperation(value = "View application", response = SavingAppOutputDto.class)
    @ApiResponses(value = {@ApiResponse(code = 200, message = "ApplicationCustomer details", response = SavingAppOutputDto.class),
            @ApiResponse(code = 404, message = "Appliation does not exist", response = Void.class)})
    public ResponseEntity<SavingAppOutputDto> getSavingApplication(@ApiParam("ApplicationCustomer id") @PathVariable("id") Long applicationId) {
        Optional<SavingApplication> applicationOptional = savingService.findApplication(applicationId);
        if (applicationOptional.isPresent()) {
            return new ResponseEntity<SavingAppOutputDto>(outputMapper.mapEntity(applicationOptional.get()), HttpStatus.OK);
        }
        return new ResponseEntity<SavingAppOutputDto>(HttpStatus.NOT_FOUND);
    }

    @RequestMapping(path = "/getSavApp/{status}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiOperation(value = "View application", response = SavingAppOutputDto.class)
    @ApiResponses(value = {@ApiResponse(code = 200, message = "ApplicationCustomer details", response = SavingAppOutputDto.class),
            @ApiResponse(code = 404, message = "Appliation does not exist", response = Void.class)})
    public ResponseEntity<List<SavingAppOutputDto>> getSavingApplicationsByStatus(@ApiParam("status") @PathVariable("status") Status status) {
        List<SavingApplication> optionalListApps = savingService.findSavingAppByStatus(status);
        List<SavingAppOutputDto> savingApps = new ArrayList<>();
        for (SavingApplication savingApp : optionalListApps) {
                savingApps.add(outputMapper.mapEntity(savingApp));
        }
        return new ResponseEntity<List<SavingAppOutputDto>>(savingApps, HttpStatus.OK);
    }

    @RequestMapping(path = "/getNewSavingApp/{status}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiOperation(value = "Get saving application", response = CurrentApplicationDto.class)
    @ApiResponses(value = {@ApiResponse(code = 200, message = "Application details", response = CurrentApplicationDto.class),
            @ApiResponse(code = 404, message = "Appliation does not exist", response = Void.class)})
    public ResponseEntity<SavingAppOutputDto> getNextSavingApplication(@ApiParam("status") @PathVariable("status") Status status) {
        Optional<SavingApplication> applicationOptional = savingService.getNextSavingtAppWithStatus(status);
        if (applicationOptional.isPresent()) {
            SavingAppOutputDto savingAppOutputDto = outputMapper.mapEntity(applicationOptional.get());
            return new ResponseEntity<SavingAppOutputDto>(savingAppOutputDto, HttpStatus.OK);
        }
        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }
}
