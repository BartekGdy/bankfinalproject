package bankService.exception;

public class UnauthorizedExcetion extends RuntimeException {

    public UnauthorizedExcetion(String message) {
        super(message);
    }
}
