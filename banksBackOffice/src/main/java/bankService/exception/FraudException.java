package bankService.exception;

/**
 * Created by RENT on 2017-11-07.
 */
public class FraudException extends RuntimeException {

    public FraudException(String message) {
        super(message);
    }
}
