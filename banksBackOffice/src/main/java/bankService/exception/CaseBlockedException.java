package bankService.exception;

public class CaseBlockedException extends RuntimeException {

    public CaseBlockedException(String message) {
        super(message);
    }
}
