package bankService.creditScore;

import bankService.exception.FraudException;
import bankService.model.*;
import bankService.model.enums.AccState;
import bankService.model.service.CustomerRecordService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import sharedData.model.enums.ProductType;

import java.util.List;

public abstract class CreditScoring<App> {

    @Autowired
    private CustomerRecordService customerService;

    public int countCreditScore(int creditScore, ProcessedApplication<App> application) {
        CustomerRecord customerRecord = application.getCustomerRecord();
        creditScore = holdingsCreditScore(creditScore, customerRecord);
        creditScore = addressCreditScore(creditScore, customerRecord);

        return creditScore;
    }

    private int holdingsCreditScore(int creditScore, CustomerRecord customerRecord) {
        List<Holding> holdings = customerRecord.getHoldings();
        int cycleCounter = 0;
        if (!holdings.isEmpty()) {
            for (Holding holding : holdings) {
                if (holding.getState().equals(AccState.FRAUD)) {
                    throw new FraudException("Had account which is fraudlent! Continuation is forbidden");
                }
                if (cycleCounter == 2) {
                    return creditScore;
                }
                if (holding.getState().equals(AccState.ACTIVE) && holding.getProductType().equals(ProductType.CURRENT)) {
                    creditScore += 150;
                    cycleCounter++;
                } else if (holding.getState().equals(AccState.ACTIVE) && holding.getProductType().equals(ProductType.SAVING)) {
                    creditScore += 50;
                    cycleCounter++;
                }
            }
        }
        return creditScore;
    }

    private int addressCreditScore(int creditScore, CustomerRecord customerRecord) {
        List<CustomerRecordAddress> addresses = customerRecord.getCustomerRecordAddresses();
        int timeWithAddress = 0;
        for (CustomerRecordAddress address : addresses) {
            timeWithAddress += address.getTimeAtAddress();
        }
        creditScore -= 150 - (timeWithAddress * 50);
        return creditScore;
    }
}
