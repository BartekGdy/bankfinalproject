package bankService.creditScore;

import bankService.model.ProcessedApplication;
import org.springframework.stereotype.Component;
import sharedData.model.CurrentApplication;
import sharedData.model.SavingApplication;
import sharedData.model.enums.EmploymentType;

@Component("saving-credit")
public class SavingCreditScore extends CreditScoring<SavingApplication> {

    @Override
    public int countCreditScore(int creditScore, ProcessedApplication<SavingApplication> application) {
        creditScore = super.countCreditScore(creditScore, application);
        SavingApplication savingApp = application.getProductApp();
        Double monthlyEarnings = savingApp.getMonthlyEarnings();
        creditScore += monthlyEarnings * 12 / 200;
        return creditScore;
    }
}
