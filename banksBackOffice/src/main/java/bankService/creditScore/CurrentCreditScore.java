package bankService.creditScore;

import bankService.model.ProcessedApplication;
import org.springframework.stereotype.Component;
import sharedData.model.CurrentApplication;
import sharedData.model.SavingApplication;
import sharedData.model.enums.EmploymentType;

@Component("current-credit")
public class CurrentCreditScore extends CreditScoring<CurrentApplication> {

    @Override
    public int countCreditScore(int creditScore, ProcessedApplication<CurrentApplication> application) {
        creditScore = super.countCreditScore(creditScore, application);
        CurrentApplication currentApp = application.getProductApp();
        creditScore = employmentTypeCreditScore(creditScore, currentApp);
        creditScore = numberOfDependentsCreditScore(creditScore, currentApp);
        return earningsCreditScore(creditScore, currentApp);
    }

    private int earningsCreditScore(int creditScore, CurrentApplication currentApp) {
        Double monthlyEarnings = currentApp.getMonthlyEarnings();
        Double overdraftAmount = currentApp.getOverdraftAmount();
        if (monthlyEarnings - overdraftAmount <= 0 || monthlyEarnings < 600) {
            return 0;
        }
        creditScore += monthlyEarnings * 12 / 200;
        if (overdraftAmount != 0) {
            creditScore -= 100 / ((monthlyEarnings - overdraftAmount) / 1000);
        }
        if (creditScore < 0) {
            return 0;
        } else if (creditScore > 1000) {
            return 1000;
        }
        return creditScore;
    }

    private int numberOfDependentsCreditScore(int creditScore, CurrentApplication currentApp) {
        int numberOfDependents = currentApp.getNumberOfDependents();
        for (int i = 0; i < numberOfDependents; i++) {
            creditScore -= 50;
        }
        return creditScore;
    }

    private int employmentTypeCreditScore(int creditScore, CurrentApplication currentApp) {
        EmploymentType employmentType = currentApp.getEmploymentType();
        if (employmentType.equals(EmploymentType.FIXED_TERM_CONTRACT)) {
            creditScore += 100;
        } else if (employmentType.equals(EmploymentType.INDEFINITE_PERIOD_CONTRACT)) {
            creditScore += 200;
        } else if (employmentType.equals(EmploymentType.INTERNSHIP)) {
            creditScore -= 200;
        } else if (employmentType.equals(EmploymentType.CONTRACT_WORK)) {
            creditScore -= 50;
        }
        return creditScore;
    }
}
