package bankService.repository;

import bankService.model.CurrentAccount;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface CurrentAccountRepository extends JpaRepository<CurrentAccount,Long>{

    Optional<CurrentAccount> findById(Long id);

    Optional<CurrentAccount>findByProductNumber(String accNumber);
}
