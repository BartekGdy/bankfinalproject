package bankService.repository;

import bankService.model.ProcessedCurrentApplication;
import bankService.model.ProcessedSavingApplication;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface ProcessedSavingAppRepository extends JpaRepository<ProcessedSavingApplication,Long> {

    Optional<ProcessedSavingApplication> findById(Long id);

    List<ProcessedSavingApplication> findByAdvisor_Username(String advisorUsername);
}
