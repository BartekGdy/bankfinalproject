package bankService.repository;


import bankService.model.AdvisorEntity;
import bankService.model.enums.Role;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface AdvisorRepository extends JpaRepository<AdvisorEntity,Long>{

    Optional<AdvisorEntity>  findById(Long id);

    Optional<AdvisorEntity> findByUsername(String username);

    List<AdvisorEntity> findByRole(Role role);
}
