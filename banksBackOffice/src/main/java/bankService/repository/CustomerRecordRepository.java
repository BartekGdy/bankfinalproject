package bankService.repository;

import bankService.model.CustomerRecord;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;
import java.util.Optional;

@Repository
public interface CustomerRecordRepository extends JpaRepository<CustomerRecord,Long>{

    Optional<CustomerRecord> findById(Long id);
    @Query("SELECT c FROM CustomerRecord c JOIN c.customerRecordAddresses a WHERE a.premise=:premise AND a.postCode =:postCode")
    List<CustomerRecord> findByPremiseAndPostCode(@Param("premise") String premise, @Param("postCode") String postCode);

    List<CustomerRecord> findAllByLastNameAndDateOfBirth(String lastName, Date dob);

    @Query("SELECT c FROM CustomerRecord c JOIN c.customerRecordAddresses a WHERE c.lastName =:lastName AND a.postCode=:postCode")
    List<CustomerRecord> findByLastNameAndPostCode(@Param("lastName") String lastName,@Param("postCode") String postCode);

    @Query("SELECT c FROM CustomerRecord c WHERE c.id = (SELECT h.customerRecord FROM bankService.model.Holding h WHERE h.productNumber =:productNumber)")
    List<CustomerRecord> findByProductNumber(@Param("productNumber") String productNumber);
}
