package bankService.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import sharedData.model.ProductApp;
import sharedData.model.enums.Status;

import java.util.List;
import java.util.Optional;

public interface ProductAppRepository extends JpaRepository<ProductApp,Long>{


    @Query("SELECT p FROM ProductApp p WHERE customer_id =:customerId")
    List<ProductApp> findAllCustomersAccounts(@Param("customerId") Long id);



    List<ProductApp> findByStatus(Status status);

    Optional<ProductApp> findById(Long id);
}
