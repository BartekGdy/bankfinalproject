package bankService.repository;

import bankService.model.CustomerRecordAddress;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import sharedData.model.CustomerAppAddress;

@Repository
public interface AddressRepository extends JpaRepository<CustomerRecordAddress,Long> {
}
