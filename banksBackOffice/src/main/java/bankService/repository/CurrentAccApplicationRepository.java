package bankService.repository;

        import org.springframework.data.jpa.repository.JpaRepository;
        import org.springframework.data.jpa.repository.Query;
        import org.springframework.data.repository.query.Param;
        import org.springframework.stereotype.Repository;
        import sharedData.model.CurrentApplication;
        import sharedData.model.SavingApplication;
        import sharedData.model.enums.Status;


        import java.util.List;
        import java.util.Optional;

@Repository
public interface CurrentAccApplicationRepository extends JpaRepository<CurrentApplication, Long> {

    Optional<CurrentApplication> findById(Long id);

    @Query("SELECT c FROM CurrentApplication c WHERE c.status = :status AND c.id NOT IN (SELECT p.productApp FROM bankService.model.ProcessedCurrentApplication p) ORDER BY c.id")
    List<CurrentApplication> findCurrentAppsByStatus(@Param("status")Status status);

    @Query("SELECT c " +
            "FROM CurrentApplication c " +
            "WHERE c.id IN " +
            "(SELECT p.productApp FROM bankService.model.ProcessedCurrentApplication p) " +
            "AND c.id = " +
            "(SELECT MIN(y.id) FROM CurrentApplication y WHERE y.status = :status)")
    Optional<CurrentApplication> findNewCurrentAppByStatus(@Param("status")Status status);

}
