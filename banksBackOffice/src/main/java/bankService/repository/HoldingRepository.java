package bankService.repository;

import bankService.model.CustomerRecord;
import bankService.model.Holding;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface HoldingRepository extends JpaRepository<Holding,Long> {

    List<Holding> findAllByCustomerRecord(CustomerRecord customer);
}
