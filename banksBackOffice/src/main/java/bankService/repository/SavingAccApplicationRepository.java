package bankService.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import sharedData.model.CurrentApplication;
import sharedData.model.SavingApplication;
import sharedData.model.enums.Status;

import java.util.List;
import java.util.Optional;

@Repository
public interface SavingAccApplicationRepository extends JpaRepository<SavingApplication,Long>{

    Optional<SavingApplication> findById(Long id);

    @Query("SELECT s FROM SavingApplication s WHERE s.status = :status AND s.id NOT IN (SELECT p.productApp FROM bankService.model.ProcessedCurrentApplication p) ORDER BY s.id")
    List<SavingApplication> findListOfSavingAppByStatus(@Param("status")Status status);

    @Query("SELECT s " +
            "FROM SavingApplication s " +
            "WHERE s.id IN " +
            "(SELECT p.productApp FROM bankService.model.ProcessedCurrentApplication p) " +
            "AND s.id = " +
            "(SELECT MIN(y.id) FROM SavingApplication y WHERE y.status = :status)")
    Optional<SavingApplication> findNewSavingAppByStatus(@Param("status")Status status);

}
