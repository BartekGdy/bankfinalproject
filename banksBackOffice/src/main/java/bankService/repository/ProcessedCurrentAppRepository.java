package bankService.repository;

import bankService.model.ProcessedCurrentApplication;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface ProcessedCurrentAppRepository extends JpaRepository<ProcessedCurrentApplication,Long> {

    Optional<ProcessedCurrentApplication> findById(Long id);

    List<ProcessedCurrentApplication> findByAdvisor_Username(String username);
}
