package bankService.repository;

import bankService.model.CurrentAccount;
import bankService.model.SavingAccount;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface SavingAccountRepository extends JpaRepository<SavingAccount,Long>{

    Optional<SavingAccount> findById(Long id);
    Optional<SavingAccount>findByProductNumber(String accNumber);

}
