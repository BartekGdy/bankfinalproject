package bankService.mapper;

import bankService.model.CurrentAccount;
import bankService.model.Holding;
import bankService.model.dto.CurrentAccountDto;
import bankService.model.dto.HoldingDto;
import org.springframework.stereotype.Component;
import sharedData.mapper.EntityMapper;

@Component("curr-acc")
public class CurrentAccMapper implements EntityMapper<CurrentAccount, CurrentAccountDto> {

    @Override
    public CurrentAccountDto mapEntity(CurrentAccount entity) {
        CurrentAccountDto dto = new CurrentAccountDto();
        dto.setProductNumber(entity.getProductNumber());
        dto.setProductType(entity.getProductType());
        dto.setState(entity.getState());
        dto.setOverdraftAmount(entity.getOverdraftAmount());
        return dto;
    }
}
