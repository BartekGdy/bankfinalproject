package bankService.mapper;

import bankService.model.SavingAccount;
import bankService.model.dto.SavingAccountDto;
import org.springframework.stereotype.Component;
import sharedData.mapper.EntityMapper;

@Component("sav-acc")
public class SavingAccMapper implements EntityMapper<SavingAccount, SavingAccountDto> {

    @Override
    public SavingAccountDto mapEntity(SavingAccount entity) {
        SavingAccountDto dto = new SavingAccountDto();
        dto.setProductNumber(entity.getProductNumber());
        dto.setProductType(entity.getProductType());
        dto.setState(entity.getState());
        dto.setInterestsRate(entity.getInterestsRate());
        return dto;
    }
}
