package bankService.mapper;

import bankService.model.CustomerRecordAddress;
import bankService.model.dto.CustRecordAddressDto;
import org.springframework.stereotype.Component;
import sharedData.mapper.Mapper;

import sharedData.model.dto.CustAppAddressDto;


@Component("address-bo")
public class BoAddressMapper implements Mapper<CustomerRecordAddress,CustRecordAddressDto> {

    public CustomerRecordAddress mapDto(CustRecordAddressDto dto) {
        CustomerRecordAddress entity = new CustomerRecordAddress();
        entity.setPremise(dto.getPremise());
        entity.setStreet(dto.getStreet());
        entity.setTown(dto.getTown());
        entity.setPostCode(dto.getPostCode());
        entity.setAddressType(dto.getAddressType());
        entity.setTimeAtAddress(dto.getTimeAtAddress());
        return entity;
    }


    public CustRecordAddressDto mapEntity(CustomerRecordAddress entity) {
        CustRecordAddressDto dto = new CustRecordAddressDto();
        dto.setPremise(entity.getPremise());
        dto.setStreet(entity.getStreet());
        dto.setTown(entity.getTown());
        dto.setPostCode(entity.getPostCode());
        dto.setAddressType(entity.getAddressType());
        dto.setTimeAtAddress(entity.getTimeAtAddress());
        return dto;
    }
}
