package bankService.mapper;

import bankService.exception.ApplicationNotFoundException;
import bankService.exception.CustomerNotFoundException;
import bankService.model.AdvisorEntity;
import bankService.model.CustomerRecord;
import bankService.model.ProcessedCurrentApplication;
import bankService.model.dto.ProcessedCurrentApplicaitonOutputDto;
import bankService.model.service.CurrentAppService;
import bankService.model.service.CustomerRecordService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import sharedData.mapper.EntityMapper;
import sharedData.model.CurrentApplication;

import java.util.Optional;

@Component(value = "cur-case-output")
public class ProcessedCurrAppOutputMapper implements EntityMapper<ProcessedCurrentApplication,ProcessedCurrentApplicaitonOutputDto> {

    @Autowired
    private CustomerRecordService customerRecordService;

    @Autowired
    private CurrentAppService applicationService;

//    @Autowired
//    @Qualifier("current-output")
//    private EntityMapper<CurrentApplication,CurrentAppOutputDto> currentAppMapper;

    @Override
    public ProcessedCurrentApplicaitonOutputDto mapEntity(ProcessedCurrentApplication entity) {
        ProcessedCurrentApplicaitonOutputDto dto = new ProcessedCurrentApplicaitonOutputDto();
        Optional<CustomerRecord> customerRecord = customerRecordService.getCustomerById(entity.getCustomerRecord().getId());
        if (!customerRecord.isPresent()) {
            throw new CustomerNotFoundException();
        }
        dto.setCustomerId(customerRecord.get().getId());
        Optional<CurrentApplication> application = applicationService.findApplication(entity.getProductApp().getId());
        if (!application.isPresent()) {
            throw new ApplicationNotFoundException();
        }
        dto.setProductType(application.get().getProduct());
        dto.setProductAppId(entity.getProductApp().getId());
        dto.setStatus(entity.getStatus());
        AdvisorEntity advisor = entity.getAdvisor();
        if (advisor!=null) {
            dto.setAdvisorUservname(advisor.getUsername());
        }
        dto.setId(entity.getId());
        dto.setProductAppId(entity.getProductApp().getId());
        return dto;
    }
}
