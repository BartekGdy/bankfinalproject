package bankService.mapper;

import bankService.exception.ApplicationNotFoundException;
import bankService.exception.CustomerNotFoundException;
import bankService.model.AdvisorEntity;
import bankService.model.CustomerRecord;
import bankService.model.ProcessedSavingApplication;
import bankService.model.dto.ProcessedSavingApplicaitonOutputDto;
import bankService.model.service.CustomerRecordService;
import bankService.model.service.SavingAppService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;
import sharedData.mapper.EntityMapper;
import sharedData.model.SavingApplication;
import sharedData.model.dto.SavingAppOutputDto;

import java.util.Optional;

@Component(value = "sav-case-output")
public class ProcessedSavAppOutputMapper implements EntityMapper<ProcessedSavingApplication,ProcessedSavingApplicaitonOutputDto> {

    @Autowired
    private CustomerRecordService customerRecordService;

    @Autowired
    private SavingAppService applicationService;

    @Autowired
    @Qualifier("saving-output")
    private EntityMapper<SavingApplication,SavingAppOutputDto> savingMapper;

    @Override
    public ProcessedSavingApplicaitonOutputDto mapEntity(ProcessedSavingApplication entity) {
        ProcessedSavingApplicaitonOutputDto dto = new ProcessedSavingApplicaitonOutputDto();
        Optional<CustomerRecord> customerRecord = customerRecordService.getCustomerById(entity.getCustomerRecord().getId());
        if (!customerRecord.isPresent()) {
            throw new CustomerNotFoundException();
        }
        dto.setCustomerId(customerRecord.get().getId());
        Optional<SavingApplication> application = applicationService.findApplication(entity.getProductApp().getId());
        if (!application.isPresent()) {
            throw new ApplicationNotFoundException();
        }
        dto.setProductType(application.get().getProduct());
        dto.setProductAppId(entity.getProductApp().getId());
        dto.setStatus(entity.getStatus());
        AdvisorEntity advisor = entity.getAdvisor();
        if (advisor!=null) {
            dto.setAdvisorUservname(advisor.getUsername());
        }
        dto.setId(entity.getId());
        return dto;
    }
}
