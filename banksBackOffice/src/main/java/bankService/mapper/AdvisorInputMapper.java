package bankService.mapper;

import bankService.model.AdvisorEntity;
import bankService.model.dto.input.AdvisorInputDto;
import org.springframework.aop.Advisor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import sharedData.converter.LocalDateAttributeConverter;
import sharedData.mapper.DtoMapper;
import sharedData.mapper.Mapper;

@Component(value = "advisor-input")
public class AdvisorInputMapper implements DtoMapper<AdvisorInputDto,AdvisorEntity>{


    @Autowired
    private LocalDateAttributeConverter dateConverter;
    @Override
    public AdvisorEntity mapDto(AdvisorInputDto dto) {
        AdvisorEntity entity = new AdvisorEntity();
        entity.setPassword(dto.getPassword());
        entity.setRole(dto.getRole());
        entity.setFirstname(dto.getFirstname());
        entity.setLastname(dto.getFirstname());
        entity.setDateOfBirth(dateConverter.convert(dto.getDateOfBirth()));
        return entity;
    }
}
