package bankService.mapper;

import bankService.model.Holding;
import bankService.model.dto.HoldingDto;
import org.springframework.stereotype.Component;
import sharedData.mapper.EntityMapper;

@Component("holding")
public class HoldingMapper implements EntityMapper<Holding, HoldingDto> {

    @Override
    public HoldingDto mapEntity(Holding holding) {
        HoldingDto dto = new HoldingDto();
        dto.setProductNumber(holding.getProductNumber());
        dto.setProductType(holding.getProductType());
        dto.setState(holding.getState());
        return dto;
    }
}
