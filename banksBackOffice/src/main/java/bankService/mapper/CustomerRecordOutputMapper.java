package bankService.mapper;

import bankService.model.CustomerRecord;
import bankService.model.CustomerRecordAddress;
import bankService.model.dto.CustRecordAddressDto;
import bankService.model.dto.CustomerRecordDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;
import sharedData.converter.LocalDateAttributeConverter;
import sharedData.mapper.EntityMapper;
import sharedData.mapper.Mapper;


import java.util.ArrayList;
import java.util.List;

/**
 * Created by RENT on 2017-11-06.
 */
@Component(value = "customer-output")
public class CustomerRecordOutputMapper implements EntityMapper<CustomerRecord,CustomerRecordDto> {
    @Autowired
    @Qualifier("address-bo")
    private Mapper<CustomerRecordAddress, CustRecordAddressDto> mapperAddress;

    @Autowired
    LocalDateAttributeConverter dateConverter;

    @Override
    public CustomerRecordDto mapEntity(CustomerRecord entity) {
        CustomerRecordDto dto = new CustomerRecordDto();
        dto.setId(entity.getId());
        dto.setEmail(entity.getEmail());
        dto.setFirstname(entity.getFirstName());
        dto.setLastname(entity.getLastName());
        dto.setPhone(entity.getPhone());
        List<CustomerRecordAddress> addresses = entity.getCustomerRecordAddresses();
        List<CustRecordAddressDto> addressesDtos = new ArrayList<>();
        for (CustomerRecordAddress address : addresses) {
            addressesDtos.add(mapperAddress.mapEntity(address));
        }
        dto.setAddresses(addressesDtos);
        dto.setGender(entity.getGender());
        dto.setDateOfBirth(dateConverter.convert(entity.getDateOfBirth()));
        return dto;
    }
}
