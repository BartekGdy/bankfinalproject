package bankService.mapper;

import bankService.model.AdvisorEntity;
import bankService.model.dto.AdvisorOutputDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import sharedData.converter.LocalDateAttributeConverter;
import sharedData.mapper.EntityMapper;

@Component(value = "advisor-output")
public class AdvisorOutputMapper implements EntityMapper<AdvisorEntity,AdvisorOutputDto> {

    @Autowired
    private LocalDateAttributeConverter dateConverter;

    @Override
    public AdvisorOutputDto mapEntity(AdvisorEntity entity) {
        AdvisorOutputDto dto = new AdvisorOutputDto();
        dto.setUsername(entity.getUsername());
        dto.setPassword(entity.getPassword());
        dto.setRole(entity.getRole());
        dto.setFirstname(entity.getFirstname());
        dto.setLastname(entity.getLastname());
        dto.setDateOfBirth(dateConverter.convert(entity.getDateOfBirth()));
        return dto;
    }
}
