package bankService.mapper;

import bankService.exception.ApplicationNotFoundException;
import bankService.exception.CustomerNotFoundException;
import bankService.model.CustomerRecord;
import bankService.model.ProcessedSavingApplication;
import bankService.model.dto.input.ProcessedSavingApplicaitonInputDto;
import bankService.model.service.CustomerRecordService;
import bankService.model.service.ProcessedSavingAppService;
import bankService.model.service.SavingAppService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import sharedData.mapper.DtoMapper;
import sharedData.model.ProductApp;
import sharedData.model.SavingApplication;

import java.util.Optional;

@Component(value = "sav-case-input")
public class ProcessedSavAppInputMapper implements DtoMapper<ProcessedSavingApplicaitonInputDto,ProcessedSavingApplication> {

    @Autowired
    private CustomerRecordService customerRecordService;

    @Autowired
    private SavingAppService applicationService;


    @Override
    public ProcessedSavingApplication mapDto(ProcessedSavingApplicaitonInputDto dto) {
        ProcessedSavingApplication entity = new ProcessedSavingApplication();
        Optional<CustomerRecord> customerRecord = customerRecordService.getCustomerById(dto.getCustomerId());
        if (!customerRecord.isPresent()) {
            throw new CustomerNotFoundException();
        }
        entity.setCustomerRecord(customerRecord.get());
        Optional<SavingApplication> application = applicationService.findApplication(dto.getProductAppId());
        if (!application.isPresent()) {
            throw new ApplicationNotFoundException();
        }
        entity.setProductApp(application.get());
        return entity;
    }
}
