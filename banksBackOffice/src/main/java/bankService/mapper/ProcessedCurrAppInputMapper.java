package bankService.mapper;

import bankService.exception.ApplicationNotFoundException;
import bankService.exception.CustomerNotFoundException;
import bankService.model.CustomerRecord;
import bankService.model.ProcessedCurrentApplication;
import bankService.model.dto.input.ProcessedCurrentApplicaitonInputDto;
import bankService.model.service.CurrentAppService;
import bankService.model.service.CustomerRecordService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import sharedData.mapper.DtoMapper;
import sharedData.model.CurrentApplication;
import sharedData.model.ProductApp;

import java.util.Optional;

@Component(value = "cur-case-input")
public class ProcessedCurrAppInputMapper implements DtoMapper<ProcessedCurrentApplicaitonInputDto, ProcessedCurrentApplication> {

    @Autowired
    private CustomerRecordService customerRecordService;

    @Autowired
    private CurrentAppService applicationService;


    @Override
    public ProcessedCurrentApplication mapDto(ProcessedCurrentApplicaitonInputDto dto) {
        ProcessedCurrentApplication entity = new ProcessedCurrentApplication();
        Optional<CustomerRecord> customerRecord = customerRecordService.getCustomerById(dto.getCustomerId());
        if (!customerRecord.isPresent()) {
            throw new CustomerNotFoundException();
        }
        entity.setCustomerRecord(customerRecord.get());
        Optional<CurrentApplication> application = applicationService.findApplication(dto.getProductAppId());
        if (!application.isPresent()) {
            throw new ApplicationNotFoundException();
        }
        entity.setProductApp(application.get());
        return entity;
    }
}
