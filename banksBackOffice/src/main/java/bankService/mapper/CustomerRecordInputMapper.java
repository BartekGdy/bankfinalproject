package bankService.mapper;

import bankService.model.CustomerRecord;
import bankService.model.CustomerRecordAddress;
import bankService.model.dto.CustRecordAddressDto;
import bankService.model.dto.input.CustomerRecordInputDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;
import sharedData.converter.LocalDateAttributeConverter;
import sharedData.mapper.DtoMapper;
import sharedData.mapper.Mapper;
import sharedData.model.dto.CustAppAddressDto;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by RENT on 2017-11-06.
 */
@Component(value = "customer-input")
public class CustomerRecordInputMapper implements DtoMapper<CustomerRecordInputDto,CustomerRecord> {
    @Autowired
    @Qualifier("address-bo")
    private Mapper<CustomerRecordAddress, CustRecordAddressDto> mapperAddress;

    @Autowired
    LocalDateAttributeConverter dateConverter;



    @Override
    public CustomerRecord mapDto(CustomerRecordInputDto dto) {
        CustomerRecord entity = new CustomerRecord();
        entity.setEmail(dto.getEmail());
        entity.setFirstName(dto.getFirstname());
        entity.setLastName(dto.getLastname());
        entity.setPhone(dto.getPhone());
        List<CustRecordAddressDto> addressesDto = dto.getAddresses();
        List<CustomerRecordAddress> addresses = new ArrayList<>();
        for (CustRecordAddressDto custAppAddressDto : addressesDto) {
            addresses.add(mapperAddress.mapDto(custAppAddressDto));
        }
        entity.setCustomerRecordAddresses(addresses);
        entity.setGender(dto.getGender());
        entity.setDateOfBirth(dateConverter.convert(dto.getDateOfBirth()));
        return entity;
    }
}
