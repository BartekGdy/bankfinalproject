package bankService;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@SpringBootApplication
@ComponentScan(basePackages = "bankService")
@ComponentScan(basePackages = "sharedData")
@EnableJpaRepositories
@EnableTransactionManagement
@EntityScan({"bankService","sharedData"})
public class ApplicationBackOffice {
//    private static ExecutorService executorService= Executors.newSingleThreadExecutor();;

    public static void main(final String... args) {
        new SpringApplication(ApplicationBackOffice.class).run(args);
//        executorService.submit(new ProcessedCurrentApplication());
    }
}
