package bankService.model;

import org.hibernate.validator.constraints.NotEmpty;
import sharedData.model.enums.AddressType;

import javax.persistence.*;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "record_address")
public class CustomerRecordAddress {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @NotEmpty
    private String premise;
    @NotEmpty
    private String street;
    @NotEmpty
    private String town;
    @NotEmpty
    private String postCode;
    @NotNull
    @Max(6)
    @Min(0)
    private int timeAtAddress;

    @Enumerated(EnumType.STRING)
    private AddressType addressType;

    @ManyToOne(cascade = CascadeType.PERSIST)
    @JoinColumn(name="CUSTOMER_RECORD_ID")
    private CustomerRecord customerRecord;

    public CustomerRecordAddress() {
    }

    public CustomerRecordAddress(String premise, String street, String town, String postCode) {
        this.premise = premise;
        this.street = street;
        this.town = town;
        this.postCode = postCode;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getPremise() {
        return premise;
    }

    public void setPremise(String premise) {
        this.premise = premise;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getTown() {
        return town;
    }

    public void setTown(String town) {
        this.town = town;
    }

    public String getPostCode() {
        return postCode;
    }

    public void setPostCode(String postCode) {
        this.postCode = postCode;
    }

    public int getTimeAtAddress() {
        return timeAtAddress;
    }

    public void setTimeAtAddress(int timeAtAddress) {
        this.timeAtAddress = timeAtAddress;
    }

    public CustomerRecord getCustomerRecord() {
        return customerRecord;
    }

    public void setCustomerRecord(CustomerRecord customerRecord) {
        this.customerRecord = customerRecord;
    }

    public AddressType getAddressType() {
        return addressType;
    }

    public void setAddressType(AddressType addressType) {
        this.addressType = addressType;
    }
}
