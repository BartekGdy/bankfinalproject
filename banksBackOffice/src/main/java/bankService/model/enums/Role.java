package bankService.model.enums;

public enum Role {
    ADMIN,CURRENT_ACC_ADVISOR,SAVING_ACC_ADVISOR
}
