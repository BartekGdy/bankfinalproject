package bankService.model.enums;

public enum  InterestRate {
    RATE(1.5);

   private Double interests;

    InterestRate(Double interests) {
        this.interests = interests;
    }

    public Double getInterests() {
        return interests;
    }

}
