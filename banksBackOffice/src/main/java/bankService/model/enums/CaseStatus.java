package bankService.model.enums;

public enum CaseStatus {
    INITIAL,IN_PROGRESS,ACCOUNT_OPENED,DECLINE,CANCELLED
}
