package bankService.model;

import sharedData.model.enums.ProductType;

import javax.persistence.Entity;

@Entity
public class CurrentAccount extends Holding{


    private Double overdraftAmount;

    public Double getOverdraftAmount() {
        return overdraftAmount;
    }

    public void setOverdraftAmount(Double overdraftAmount) {
        this.overdraftAmount = overdraftAmount;
    }

    public Long getId() {
        return super.getId();
    }

    public void setId(Long id) {
        super.setId(id);
    }

    public ProductType getProductType() {
        return super.getProductType();
    }

    public void setProductType(ProductType productType) {
        super.setProductType(productType);
    }

    public String getProductNumber() {
        return super.getProductNumber();
    }

    public void setProductNumber(String productNumber) {
        super.setProductNumber(productNumber);
    }


}
