package bankService.model;

import bankService.model.enums.AccState;
import sharedData.model.enums.ProductType;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Entity
@Inheritance
public abstract class Holding {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Enumerated(EnumType.STRING)
    private ProductType productType;

    @Column(unique = true)
    private String productNumber;

    @ManyToOne
    @JoinColumn(name = "CUSTOMER_ID")
    private CustomerRecord customerRecord;

    @Enumerated(EnumType.STRING)
    private AccState state;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public ProductType getProductType() {
        return productType;
    }

    public void setProductType(ProductType productType) {
        this.productType = productType;
    }

    public String getProductNumber() {
        return productNumber;
    }

    public void setProductNumber(String productNumber) {
        this.productNumber = productNumber;
    }

    public CustomerRecord getCustomerRecord() {
        return customerRecord;
    }

    public void setCustomerRecord(CustomerRecord customerRecord) {
        this.customerRecord = customerRecord;
    }

    public AccState getState() {
        return state;
    }

    public void setState(AccState state) {
        this.state = state;
    }
}
