package bankService.model.service;

import bankService.model.CustomerRecord;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;
import java.util.Optional;

public interface CustomerRecordService {
    Optional<CustomerRecord> getCustomerById(Long id);

    @Transactional
    Optional<CustomerRecord> createCustomer(CustomerRecord customerRecord);

    @Transactional
    Optional<CustomerRecord> showCustomerRecord(Long id);

    @Transactional
    CustomerRecord editCustomerRecord(Long id, CustomerRecord record);

    List<CustomerRecord> findCustomerByPremiseAndPostCode(String premise, String postCode);

    List<CustomerRecord> findCustomerByLastNameAndPostCode(String lastName, String postCode);

    List<CustomerRecord> findCustomerByLastNameAndDob(String lastName, Date dob);

    List<CustomerRecord> findCustomerByProductNumber(String productNumber);
}
