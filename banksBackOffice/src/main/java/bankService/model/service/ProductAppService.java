package bankService.model.service;

import sharedData.model.ProductApp;

import java.util.List;
import java.util.Optional;

public interface ProductAppService {

    Optional<ProductApp> getApplicationById(Long id);
}
