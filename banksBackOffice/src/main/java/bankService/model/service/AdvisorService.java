package bankService.model.service;

import bankService.model.AdvisorEntity;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

import java.util.Optional;

public interface AdvisorService extends UserDetailsService {
    Optional<AdvisorEntity> createAdvisor(AdvisorEntity advisor);

    Optional<AdvisorEntity> findByUsername(String username);

    UserDetails loadUserByUsername(String username) throws UsernameNotFoundException;

}
