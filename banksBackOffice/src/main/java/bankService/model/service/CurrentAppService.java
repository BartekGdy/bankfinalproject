package bankService.model.service;

import org.springframework.transaction.annotation.Transactional;
import sharedData.model.CurrentApplication;
import sharedData.model.enums.Status;

import java.util.List;
import java.util.Optional;

public interface CurrentAppService {

    Optional<CurrentApplication> findApplication(Long applicaionId);

    List<CurrentApplication> getCurrentsAppsWithStatus(Status status);

    Optional<CurrentApplication> getNextCurrentAppWithStatus(Status status);
}
