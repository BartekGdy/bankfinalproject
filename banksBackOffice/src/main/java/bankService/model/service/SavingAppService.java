package bankService.model.service;

import org.springframework.transaction.annotation.Transactional;
import sharedData.model.SavingApplication;
import sharedData.model.enums.Status;

import java.util.List;
import java.util.Optional;

public interface SavingAppService {

    Optional<SavingApplication> findApplication(Long applicaionId);


    List<SavingApplication> findSavingAppByStatus(Status status);

    Optional<SavingApplication> getNextSavingtAppWithStatus(Status status);
}
