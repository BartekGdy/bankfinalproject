package bankService.model.service;

import bankService.model.ProcessedCurrentApplication;
import bankService.model.ProcessedSavingApplication;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * Created by RENT on 2017-11-07.
 */
public interface ProcessedSavingAppService {
    @Transactional
    Optional<ProcessedSavingApplication> createNewCase(ProcessedSavingApplication appCase);

    @Transactional
    Optional<ProcessedSavingApplication> findById(Long id);

    ProcessedSavingApplication exitCase(Long caseId, String advisorUsername);

    ProcessedSavingApplication assignCase(Long caseId, String adviorUsername);

    ProcessedSavingApplication processCase(String advisorUsername, ProcessedSavingApplication processedCase);

    ProcessedSavingApplication cancelCase(Long caseId, String advisorUsername);
}
