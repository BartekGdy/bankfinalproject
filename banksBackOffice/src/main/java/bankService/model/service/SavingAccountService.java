package bankService.model.service;

import bankService.model.ProcessedSavingApplication;
import bankService.model.SavingAccount;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

public interface SavingAccountService {
    @Transactional
    SavingAccount generateCurrentAccount(ProcessedSavingApplication processedCase);

    @Transactional
    SavingAccount saveAccount(Long customerId, SavingAccount account);

    Optional<SavingAccount> findByAccNumber(String accNumber);
}
