package bankService.model.service.implementation;

import bankService.exception.AdvisorNotFoundException;
import bankService.model.AdvisorEntity;
import bankService.model.enums.Role;
import bankService.model.service.AdvisorService;
import bankService.repository.AdvisorRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import sharedData.converter.LocalDateAttributeConverter;

import javax.annotation.PostConstruct;
import javax.xml.ws.http.HTTPException;
import java.time.LocalDate;
import java.util.*;

@Service
public class AdvisorServiceImpl implements AdvisorService {

    @Autowired
    private AdvisorRepository advisorRepository;

    @Autowired
    private LocalDateAttributeConverter dateConverter;

    public AdvisorServiceImpl() {

    }

    @PostConstruct
    private void generateAdmin() {
        if (advisorRepository.findByRole(Role.ADMIN).isEmpty()) {
            AdvisorEntity advisor = new AdvisorEntity();
            advisor.setRole(Role.ADMIN);
            advisor.setPassword("admin123");
            advisor.setFirstname("Bartosz");
            advisor.setLastname("Szymocha");
            advisor.setDateOfBirth(dateConverter.convert(LocalDate.of(1986, 8, 19)));
            advisorRepository.save(advisor);
            setDefaultUsername(advisor);
            advisorRepository.save(advisor);

        }
    }

    @Transactional
    @Override
    public Optional<AdvisorEntity> createAdvisor(AdvisorEntity advisor) {
        AdvisorEntity savedAdvisor = advisorRepository.save(advisor);
        setDefaultUsername(advisor);
        advisorRepository.save(savedAdvisor);
        return Optional.ofNullable(savedAdvisor);
    }

    @Transactional
    @Override
    public Optional<AdvisorEntity> findByUsername(String username) {
        return advisorRepository.findByUsername(username);
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        Optional<AdvisorEntity> entityOptional = advisorRepository.findByUsername(username);
        if (!entityOptional.isPresent()) {
            throw new UsernameNotFoundException("can't find username");
        }
        AdvisorEntity advisorEntity = entityOptional.get();
        return new org.springframework.security.core.userdetails.User(
                advisorEntity.getUsername(),
                advisorEntity.getPassword(),
                true,
                true,
                true,
                true,
                mapRoles(advisorEntity));
    }

    private Collection<GrantedAuthority> mapRoles(AdvisorEntity advisor) {
        List<GrantedAuthority> roles = new ArrayList<>();
        if (advisor.getRole().equals(Role.ADMIN)) {
            roles.add(new SimpleGrantedAuthority("ROLE_ADMIN"));
        }else if(advisor.getRole().equals(Role.CURRENT_ACC_ADVISOR)){
            roles.add(new SimpleGrantedAuthority("ROLE_CURRENT_ACC_ADVISOR"));
        } else if(advisor.getRole().equals(Role.SAVING_ACC_ADVISOR)){
            roles.add(new SimpleGrantedAuthority("ROLE_SAVING_ACC_ADVISOR"));
        } else {
            roles.add(new SimpleGrantedAuthority("ROLE_USER"));
        }
        return roles;
    }

    public void setDefaultUsername(AdvisorEntity advisorEntity) {
        String numericPart = String.format("%05d", advisorEntity.getId());
        advisorEntity.setUsername(advisorEntity.getRole().name().substring(0, 3) + numericPart);
    }
}
