package bankService.model.service.implementation;

import bankService.exception.CustomerNotFoundException;
import bankService.model.*;
import bankService.model.enums.AccState;
import bankService.model.service.CurrentAccountService;
import bankService.model.service.CustomerRecordService;
import bankService.repository.CurrentAccountRepository;
import bankService.repository.CustomerRecordRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import sharedData.model.CurrentApplication;
import sharedData.model.enums.ProductType;

import java.util.List;
import java.util.Optional;

@Service
public class CurrentAccountServiceImpl implements CurrentAccountService {

    @Autowired
    private CurrentAccountRepository accountRepository;
    @Autowired
    private CustomerRecordService recordService;
    @Autowired
    private CustomerRecordRepository recordRepository;

    @Override
    @Transactional
    public CurrentAccount generateCurrentAccount(ProcessedCurrentApplication processedCase){
        CurrentAccount currentAccount = new CurrentAccount();
        CustomerRecord customerRecord = processedCase.getCustomerRecord();
        CurrentApplication productApp = processedCase.getProductApp();
        currentAccount.setOverdraftAmount(processedCase.getProductApp().getOverdraftAmount());
        currentAccount.setProductType(productApp.getProduct());
        currentAccount.setCustomerRecord(customerRecord);
        currentAccount.setState(AccState.ACTIVE);
        accountRepository.save(currentAccount);
        currentAccount.setProductNumber(generateAccountNumber(currentAccount));
        accountRepository.save(currentAccount);
        currentAccount = saveAccount(customerRecord.getId(), currentAccount);
        return currentAccount;
    }
    @Override
    @Transactional
    public CurrentAccount saveAccount(Long customerId, CurrentAccount account) {
        Optional<CustomerRecord> recordOptional = recordService.getCustomerById(customerId);
        if (!recordOptional.isPresent()) {
            throw new CustomerNotFoundException();
        }
        CustomerRecord customerRecord = recordOptional.get();
        List<Holding> customerHoldings = customerRecord.getHoldings();
        customerHoldings.add(account);
        recordRepository.save(customerRecord);
        return account;
    }

    private String generateAccountNumber(CurrentAccount account) {
        return String.format("045012 %08d", account.getId());
    }
    @Override
    @Transactional
    public Optional<CurrentAccount> findByAccNumber(String accNumber){
        return accountRepository.findByProductNumber(accNumber);
    }
}
