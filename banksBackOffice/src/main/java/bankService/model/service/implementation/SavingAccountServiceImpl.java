package bankService.model.service.implementation;

import bankService.exception.CustomerNotFoundException;
import bankService.model.*;
import bankService.model.enums.AccState;
import bankService.model.enums.InterestRate;
import bankService.model.service.CustomerRecordService;
import bankService.model.service.SavingAccountService;
import bankService.repository.CustomerRecordRepository;
import bankService.repository.SavingAccountRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import sharedData.model.SavingApplication;

import java.util.List;
import java.util.Optional;

@Service
public class SavingAccountServiceImpl implements SavingAccountService {

    @Autowired
    private SavingAccountRepository accountRepository;
    @Autowired
    private CustomerRecordService recordService;
    @Autowired
    private CustomerRecordRepository recordRepository;

    @Override
    @Transactional
    public SavingAccount generateCurrentAccount(ProcessedSavingApplication processedCase){
        SavingAccount savingAccount = new SavingAccount();
        CustomerRecord customerRecord = processedCase.getCustomerRecord();
        SavingApplication productApp = processedCase.getProductApp();
        savingAccount.setProductType(productApp.getProduct());
        savingAccount.setCustomerRecord(customerRecord);
        savingAccount.setInterestsRate(InterestRate.RATE.getInterests());
        savingAccount.setState(AccState.ACTIVE);
        accountRepository.save(savingAccount);
        savingAccount.setProductNumber(generateAccountNumber(savingAccount));
        accountRepository.save(savingAccount);
        savingAccount = saveAccount(customerRecord.getId(), savingAccount);
        return savingAccount;
    }
    @Override
    @Transactional
    public SavingAccount saveAccount(Long customerId, SavingAccount account) {
        Optional<CustomerRecord> recordOptional = recordService.getCustomerById(customerId);
        if (!recordOptional.isPresent()) {
            throw new CustomerNotFoundException();
        }
        CustomerRecord customerRecord = recordOptional.get();
        List<Holding> customerHoldings = customerRecord.getHoldings();
        customerHoldings.add(account);
        recordRepository.save(customerRecord);
        return account;
    }

    private String generateAccountNumber(SavingAccount account) {
        return String.format("045011 %08d", account.getId());
    }
    @Override
    @Transactional
    public Optional<SavingAccount> findByAccNumber(String accNumber){
        return accountRepository.findByProductNumber(accNumber);
    }
}
