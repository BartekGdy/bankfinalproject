package bankService.model.service.implementation;

import bankService.model.service.CurrentAppService;
import bankService.repository.CurrentAccApplicationRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import sharedData.model.CurrentApplication;
import sharedData.model.enums.Status;

import java.util.List;
import java.util.Optional;

@Service
public class CurrentAppServiceImpl implements CurrentAppService {

    @Autowired
    private CurrentAccApplicationRepository currentAccRepository;

    @Override
    public Optional<CurrentApplication> findApplication(Long applicaionId){
        return currentAccRepository.findById(applicaionId);
    }

    @Override
    @Transactional
    public List<CurrentApplication> getCurrentsAppsWithStatus(Status status){
        return currentAccRepository.findCurrentAppsByStatus(status);
    }

    @Override
    public Optional<CurrentApplication> getNextCurrentAppWithStatus(Status status) {
        return currentAccRepository.findNewCurrentAppByStatus(status);
    }

}

