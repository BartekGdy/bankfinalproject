package bankService.model.service.implementation;

import bankService.model.service.ProductAppService;
import bankService.repository.ProductAppRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import sharedData.model.CustomerDetails;
import sharedData.model.ProductApp;
import sharedData.model.enums.Status;

import java.util.List;
import java.util.Optional;

@Service
public class ProductAppServiceImpl implements ProductAppService {

    @Autowired
    private ProductAppRepository productAppRepository;

    @Override
    public Optional<ProductApp> getApplicationById(Long id) {
        return productAppRepository.findById(id);

    }


}
