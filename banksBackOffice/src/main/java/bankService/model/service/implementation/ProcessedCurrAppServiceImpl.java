package bankService.model.service.implementation;

import bankService.creditScore.CurrentCreditScore;
import bankService.exception.AdvisorNotFoundException;
import bankService.exception.CaseBlockedException;
import bankService.exception.CaseNotFoundException;
import bankService.exception.UnauthorizedExcetion;
import bankService.model.AdvisorEntity;
import bankService.model.CurrentAccount;
import bankService.model.CustomerRecord;
import bankService.model.ProcessedCurrentApplication;
import bankService.model.enums.CaseStatus;
import bankService.model.service.AdvisorService;
import bankService.model.service.CurrentAccountService;
import bankService.model.service.ProcessedCurrAppService;
import bankService.repository.ProcessedCurrentAppRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import sharedData.model.CurrentApplication;
import sharedData.model.enums.Status;

import java.util.List;
import java.util.Optional;

@Service
public class ProcessedCurrAppServiceImpl implements ProcessedCurrAppService {

    @Autowired
    private ProcessedCurrentAppRepository caseRepository;
    @Autowired
    private AdvisorService advisorService;
    @Autowired
    private CurrentCreditScore creditScore;
    @Autowired
    private CurrentAccountService accountService;
    @Override
    @Transactional
    public Optional<ProcessedCurrentApplication> createNewCase(ProcessedCurrentApplication appCase) {
        appCase.setStatus(CaseStatus.IN_PROGRESS);
        appCase.getProductApp().setStatus(Status.PENDING);
        ProcessedCurrentApplication savedCase = caseRepository.save(appCase);
        return Optional.ofNullable(savedCase);
    }

    @Override
    @Transactional
    public Optional<ProcessedCurrentApplication> findById(Long id) {
        return caseRepository.findById(id);
    }

    @Override
    @Transactional
    public ProcessedCurrentApplication exitCase(Long caseId, String advisorUsername) {
        Optional<ProcessedCurrentApplication> caseOptional = caseRepository.findById(caseId);
        if (!caseOptional.isPresent()) {
            throw new CaseNotFoundException();
        }
        ProcessedCurrentApplication processedCase = caseOptional.get();
        List<ProcessedCurrentApplication> advisorCases = caseRepository.findByAdvisor_Username(advisorUsername);
        if (processedCase.getStatus().equals(CaseStatus.IN_PROGRESS) && advisorCases.contains(processedCase)) {
            processedCase.setAdvisor(null);
            processedCase.setStatus(CaseStatus.INITIAL);
            caseRepository.save(processedCase);
            return processedCase;
        }
        AdvisorEntity advisor = processedCase.getAdvisor();
        if (advisor != null && processedCase.getStatus().equals(CaseStatus.IN_PROGRESS)) {
            throw new CaseBlockedException("Case blocked by " + advisor.getUsername());
        }
        if (!processedCase.getStatus().equals(CaseStatus.INITIAL)){
            throw new CaseBlockedException("Case is no longer available for processing");
        }
        throw new CaseBlockedException("Yoy are not assgin to this case");
    }

    @Override
    @Transactional
    public ProcessedCurrentApplication assignCase(Long caseId, String adviorUsername) {
        Optional<ProcessedCurrentApplication> caseOptional = caseRepository.findById(caseId);
        if (!caseOptional.isPresent()) {
            throw new CaseNotFoundException();
        }
        ProcessedCurrentApplication processedCase = caseOptional.get();
        if (processedCase.getStatus().equals(CaseStatus.IN_PROGRESS)){
            throw new CaseBlockedException("Case is currently locked by " +processedCase.getAdvisor().getUsername());
        } else if (!processedCase.getStatus().equals(CaseStatus.INITIAL)){
            throw new CaseBlockedException("Case is no longer available for processing");
        }
        processedCase.setStatus(CaseStatus.IN_PROGRESS);
        if (processedCase.getAdvisor() == null) {
            Optional<AdvisorEntity> advisor = advisorService.findByUsername(adviorUsername);
            if (!advisor.isPresent()) {
                throw new AdvisorNotFoundException();
            }
            processedCase.setAdvisor(advisor.get());
            caseRepository.save(processedCase);
        }
        return processedCase;
    }

    @Override
    @Transactional
    public ProcessedCurrentApplication processCase(String advisorUsername, ProcessedCurrentApplication processedCase){
        AdvisorEntity advisor = processedCase.getAdvisor();
        if (advisor==null){
            throw new AdvisorNotFoundException();
        }
        if (!advisor.getUsername().equals(advisorUsername) && processedCase.getStatus().equals(CaseStatus.IN_PROGRESS)){
            throw new CaseBlockedException("This case is currently processed by " +advisor.getUsername());
        }
        if (!processedCase.getStatus().equals(CaseStatus.INITIAL) && !processedCase.getStatus().equals(CaseStatus.IN_PROGRESS)){
            throw new CaseBlockedException("Case is no longer available for processing");
        }
        processedCase.setStatus(CaseStatus.IN_PROGRESS);
        caseRepository.save(processedCase);
        int creditScore = this.creditScore.countCreditScore(500, processedCase);
        System.out.println(creditScore);
        if (creditScore<300){
            processedCase.setStatus(CaseStatus.DECLINE);
            processedCase.getProductApp().setStatus(Status.DECLINE);
        }else {
            processedCase.setStatus(CaseStatus.ACCOUNT_OPENED);
            CurrentAccount currentAccount = accountService.generateCurrentAccount(processedCase);
            processedCase.getProductApp().setStatus(Status.ACCOUNT_OPENED);
        }
        caseRepository.save(processedCase);
        return processedCase;
    }
    @Override
    @Transactional
    public ProcessedCurrentApplication cancelCase(Long caseId, String advisorUsername) {
        Optional<ProcessedCurrentApplication> caseOptional = caseRepository.findById(caseId);
        if (!caseOptional.isPresent()) {
            throw new CaseNotFoundException();
        }
        ProcessedCurrentApplication processedCase = caseOptional.get();
        List<ProcessedCurrentApplication> advisorCases = caseRepository.findByAdvisor_Username(advisorUsername);
        if (processedCase.getStatus().equals(CaseStatus.IN_PROGRESS) && advisorCases.contains(processedCase)) {
            processedCase.setStatus(CaseStatus.CANCELLED);
            processedCase.getProductApp().setStatus(Status.CANCELLED);
            caseRepository.save(processedCase);
            return processedCase;
        }
        AdvisorEntity advisor = processedCase.getAdvisor();
        if (advisor != null && processedCase.getStatus().equals(CaseStatus.IN_PROGRESS)) {
            throw new CaseBlockedException("Case blocked by " + advisor.getUsername());
        }
        if (!processedCase.getStatus().equals(CaseStatus.INITIAL)){
            throw new CaseBlockedException("Case is no longer available for processing");
        }
        throw new CaseBlockedException("Yoy are not assgin to this case");
    }
}
