package bankService.model.service.implementation;

import bankService.model.CustomerRecord;
import bankService.model.Holding;
import bankService.model.service.HoldingService;
import bankService.repository.HoldingRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
public class HoldingServiceImpl implements HoldingService {

    @Autowired
    private HoldingRepository holdingRepository;

    @Override
    @Transactional
    public List<Holding> getAllHoldingsForCustomer(CustomerRecord customer){
       return holdingRepository.findAllByCustomerRecord(customer);
    }
}
