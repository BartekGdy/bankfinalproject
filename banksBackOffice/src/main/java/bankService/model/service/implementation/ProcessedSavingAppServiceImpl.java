package bankService.model.service.implementation;

import bankService.creditScore.CreditScoring;
import bankService.creditScore.CurrentCreditScore;
import bankService.creditScore.SavingCreditScore;
import bankService.exception.AdvisorNotFoundException;
import bankService.exception.CaseBlockedException;
import bankService.exception.CaseNotFoundException;
import bankService.exception.UnauthorizedExcetion;
import bankService.model.*;
import bankService.model.enums.CaseStatus;
import bankService.model.service.AdvisorService;
import bankService.model.service.ProcessedCurrAppService;
import bankService.model.service.ProcessedSavingAppService;
import bankService.model.service.SavingAccountService;
import bankService.repository.ProcessedCurrentAppRepository;
import bankService.repository.ProcessedSavingAppRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import sharedData.model.enums.Status;

import java.util.List;
import java.util.Optional;

@Service
public class ProcessedSavingAppServiceImpl implements ProcessedSavingAppService {

    @Autowired
    private ProcessedSavingAppRepository caseRepository;

    @Autowired
    private AdvisorService advisorService;
    @Autowired
    @Qualifier("saving-credit")
    private SavingCreditScore creditScore;
    @Autowired
    private SavingAccountService accountService;

    @Override
    @Transactional
    public Optional<ProcessedSavingApplication> createNewCase(ProcessedSavingApplication appCase) {
        appCase.setStatus(CaseStatus.IN_PROGRESS);
        appCase.getProductApp().setStatus(Status.PENDING);
        ProcessedSavingApplication savedCase = caseRepository.save(appCase);
        return Optional.ofNullable(savedCase);
    }
    @Override
    @Transactional
    public Optional<ProcessedSavingApplication> findById(Long id){
        return caseRepository.findById(id);
    }

    @Override
    @Transactional
    public ProcessedSavingApplication exitCase(Long caseId, String advisorUsername) {
        Optional<ProcessedSavingApplication> caseOptional = caseRepository.findById(caseId);
        if (!caseOptional.isPresent()) {
            throw new CaseNotFoundException();
        }
        ProcessedSavingApplication processedCase = caseOptional.get();
        List<ProcessedSavingApplication> advisorCases = caseRepository.findByAdvisor_Username(advisorUsername);
        if (processedCase.getStatus().equals(CaseStatus.IN_PROGRESS) && advisorCases.contains(processedCase)) {
            processedCase.setAdvisor(null);
            processedCase.setStatus(CaseStatus.INITIAL);
            caseRepository.save(processedCase);
            return processedCase;
        }
        AdvisorEntity advisor = processedCase.getAdvisor();
        if (advisor != null && processedCase.getStatus().equals(CaseStatus.IN_PROGRESS)) {
            throw new CaseBlockedException("Case blocked by " + advisor.getUsername());
        }
        if (!processedCase.getStatus().equals(CaseStatus.INITIAL)) {
            throw new CaseBlockedException("Case is no longer available for processing");
        }
        throw new CaseBlockedException("Yoy are not assgin to this case");
    }

    @Override
    @Transactional
    public ProcessedSavingApplication assignCase(Long caseId, String adviorUsername){
        Optional<ProcessedSavingApplication> caseOptional = caseRepository.findById(caseId);
        if (!caseOptional.isPresent()){
            throw new CaseNotFoundException();
        }
        ProcessedSavingApplication processedCase = caseOptional.get();
        if (processedCase.getStatus().equals(CaseStatus.IN_PROGRESS)){
            throw new CaseBlockedException("Case is currently locked by " +processedCase.getAdvisor().getUsername());
        } else if (!processedCase.getStatus().equals(CaseStatus.INITIAL)){
            throw new CaseBlockedException("Case is no longer available for processing");
        }
        processedCase.setStatus(CaseStatus.IN_PROGRESS);
        if (processedCase.getAdvisor() == null) {
            Optional<AdvisorEntity> advisor = advisorService.findByUsername(adviorUsername);
            if (!advisor.isPresent()) {
                throw new AdvisorNotFoundException();
            }
            processedCase.setAdvisor(advisor.get());
            caseRepository.save(processedCase);
        }
        return processedCase;
    }

    @Override
    @Transactional
    public ProcessedSavingApplication processCase(String advisorUsername, ProcessedSavingApplication processedCase){
        AdvisorEntity advisor = processedCase.getAdvisor();
        if (advisor==null){
            throw new AdvisorNotFoundException();
        }
        if (!advisor.getUsername().equals(advisorUsername) && processedCase.getStatus().equals(CaseStatus.IN_PROGRESS)){
            throw new CaseBlockedException("This case is currently processed by " +advisor.getUsername());
        }
        if (!processedCase.getStatus().equals(CaseStatus.INITIAL) && !processedCase.getStatus().equals(CaseStatus.IN_PROGRESS)){
            throw new CaseBlockedException("Case is no longer available for processing");
        }
        processedCase.setStatus(CaseStatus.IN_PROGRESS);
        caseRepository.save(processedCase);
        int creditScore = this.creditScore.countCreditScore(500, processedCase);
        System.out.println(creditScore);
        if (creditScore<300){
            processedCase.setStatus(CaseStatus.DECLINE);
            processedCase.getProductApp().setStatus(Status.DECLINE);
        }else {
            processedCase.setStatus(CaseStatus.ACCOUNT_OPENED);
            SavingAccount savingAccount = accountService.generateCurrentAccount(processedCase);
            processedCase.getProductApp().setStatus(Status.ACCOUNT_OPENED);
        }
        caseRepository.save(processedCase);
        return processedCase;
    }
    @Override
    @Transactional
    public ProcessedSavingApplication cancelCase(Long caseId, String advisorUsername) {
        Optional<ProcessedSavingApplication> caseOptional = caseRepository.findById(caseId);
        if (!caseOptional.isPresent()) {
            throw new CaseNotFoundException();
        }
        ProcessedSavingApplication processedCase = caseOptional.get();
        List<ProcessedSavingApplication> advisorCases = caseRepository.findByAdvisor_Username(advisorUsername);
        if (processedCase.getStatus().equals(CaseStatus.IN_PROGRESS) && advisorCases.contains(processedCase)) {
            processedCase.setStatus(CaseStatus.CANCELLED);
            processedCase.getProductApp().setStatus(Status.CANCELLED);
            caseRepository.save(processedCase);
            return processedCase;
        }
        AdvisorEntity advisor = processedCase.getAdvisor();
        if (advisor != null && processedCase.getStatus().equals(CaseStatus.IN_PROGRESS)) {
            throw new CaseBlockedException("Case blocked by " + advisor.getUsername());
        }
        if (!processedCase.getStatus().equals(CaseStatus.INITIAL)) {
            throw new CaseBlockedException("Case is no longer available for processing");
        }
        throw new CaseBlockedException("Yoy are not assgin to this case");
    }
}
