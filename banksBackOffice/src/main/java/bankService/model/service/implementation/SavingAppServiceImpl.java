package bankService.model.service.implementation;

import bankService.model.service.SavingAppService;
import bankService.repository.SavingAccApplicationRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import sharedData.model.SavingApplication;
import sharedData.model.enums.Status;

import java.util.List;
import java.util.Optional;

@Service
public class SavingAppServiceImpl implements SavingAppService {

    @Autowired
    private SavingAccApplicationRepository savingAccRepository;

    @Override
    @Transactional
    public Optional<SavingApplication> findApplication(Long applicaionId) {
        return savingAccRepository.findById(applicaionId);
    }
    @Override
    @Transactional
    public List<SavingApplication> findSavingAppByStatus(Status status){
        return savingAccRepository.findListOfSavingAppByStatus(status);
    }

    @Override
    public Optional<SavingApplication> getNextSavingtAppWithStatus(Status status) {
        return savingAccRepository.findNewSavingAppByStatus(status);
    }
}

