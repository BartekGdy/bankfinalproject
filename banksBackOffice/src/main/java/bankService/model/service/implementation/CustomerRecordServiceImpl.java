package bankService.model.service.implementation;

import bankService.exception.CustomerNotFoundException;
import bankService.model.CustomerRecord;
import bankService.model.CustomerRecordAddress;
import bankService.repository.AddressRepository;
import bankService.repository.CustomerRecordRepository;
import bankService.model.service.CustomerRecordService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import sharedData.model.CustomerAppAddress;

import java.util.Date;
import java.util.List;
import java.util.Optional;

@Service
public class CustomerRecordServiceImpl implements CustomerRecordService {

    @Autowired
    CustomerRecordRepository customerRecordRepository;
    @Autowired
    private AddressRepository addressRepository;

    @Override
    @Transactional
    public Optional<CustomerRecord> getCustomerById(Long id) {
        return customerRecordRepository.findById(id);
    }

    @Override
    @Transactional
    public Optional<CustomerRecord> createCustomer(CustomerRecord customerRecord) {
        CustomerRecord savedCustomer = customerRecordRepository.save(customerRecord);
        if (savedCustomer != null) {
            List<CustomerRecordAddress> customerAddresses = customerRecord.getCustomerRecordAddresses();
            for (CustomerRecordAddress customerAddress : customerAddresses) {
                customerAddress.setCustomerRecord(customerRecord);
                addressRepository.save(customerAddress);
            }
        }
        return Optional.ofNullable(savedCustomer);
    }

    @Override
    @Transactional
    public Optional<CustomerRecord> showCustomerRecord(Long id) {
        return customerRecordRepository.findById(id);
    }

    @Override
    @Transactional
    public CustomerRecord editCustomerRecord(Long id, CustomerRecord edditedRecord) {
        Optional<CustomerRecord> recordOptional = customerRecordRepository.findById(id);
        if (!recordOptional.isPresent() ||edditedRecord==null) {
            throw new CustomerNotFoundException();
        }
        CustomerRecord customerRecord = recordOptional.get();
        List<CustomerRecordAddress> customerAddresses = customerRecord.getCustomerRecordAddresses();
        List<CustomerRecordAddress> edditedAddresses = edditedRecord.getCustomerRecordAddresses();
        for (int i = 0; i < edditedAddresses.size(); i++) {
            customerAddresses.get(i).setAddressType(edditedAddresses.get(i).getAddressType());
            customerAddresses.get(i).setPostCode(edditedAddresses.get(i).getPostCode());
            customerAddresses.get(i).setPremise(edditedAddresses.get(i).getPremise());
            customerAddresses.get(i).setStreet(edditedAddresses.get(i).getStreet());
            customerAddresses.get(i).setTown(edditedAddresses.get(i).getStreet());
            customerAddresses.get(i).setTimeAtAddress(edditedAddresses.get(i).getTimeAtAddress());
        }
        customerRecord.setFirstName(edditedRecord.getFirstName());
        customerRecord.setCustomerRecordAddresses(edditedRecord.getCustomerRecordAddresses());
        customerRecord.setDateOfBirth(edditedRecord.getDateOfBirth());
        customerRecord.setGender(edditedRecord.getGender());
        customerRecord.setEmail(edditedRecord.getEmail());
        customerRecord.setLastName(edditedRecord.getLastName());
        customerRecord.setPhone(edditedRecord.getPhone());

        customerRecordRepository.save(customerRecord);
        return customerRecord;
    }
    @Override
    @Transactional
    public List<CustomerRecord> findCustomerByPremiseAndPostCode(String premise, String postCode){
        return customerRecordRepository.findByPremiseAndPostCode(premise,postCode);
    }
    @Override
    @Transactional
    public List<CustomerRecord> findCustomerByLastNameAndPostCode(String lastName, String postCode){
        return customerRecordRepository.findByLastNameAndPostCode(lastName,postCode);
    }
    @Override
    @Transactional
    public List<CustomerRecord> findCustomerByLastNameAndDob(String lastName, Date dob){
        return customerRecordRepository.findAllByLastNameAndDateOfBirth(lastName,dob);
    }
    @Override
    @Transactional
    public List<CustomerRecord> findCustomerByProductNumber(String productNumber){
        return customerRecordRepository.findByProductNumber(productNumber);
    }
}
