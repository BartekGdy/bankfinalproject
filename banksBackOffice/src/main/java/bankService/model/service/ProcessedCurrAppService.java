package bankService.model.service;

import bankService.model.ProcessedCurrentApplication;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

public interface ProcessedCurrAppService {
    Optional<ProcessedCurrentApplication> createNewCase(ProcessedCurrentApplication appCase);

    Optional<ProcessedCurrentApplication> findById(Long id);


    @Transactional
    ProcessedCurrentApplication exitCase(Long caseId, String advisorUsername);

    ProcessedCurrentApplication assignCase(Long caseId, String adviorUsername);

    ProcessedCurrentApplication processCase(String advisorUsername, ProcessedCurrentApplication processedCase);

    ProcessedCurrentApplication cancelCase(Long caseId, String advisorUsername);
}
