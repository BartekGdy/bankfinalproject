package bankService.model.service;

import bankService.model.CustomerRecord;
import bankService.model.Holding;

import java.util.List;

public interface HoldingService {
    List<Holding> getAllHoldingsForCustomer(CustomerRecord customer);
}
