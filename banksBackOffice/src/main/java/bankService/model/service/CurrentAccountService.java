package bankService.model.service;

import bankService.model.CurrentAccount;
import bankService.model.ProcessedCurrentApplication;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;


public interface CurrentAccountService {
    CurrentAccount generateCurrentAccount(ProcessedCurrentApplication processedCase);

    @Transactional
    CurrentAccount saveAccount(Long customerId, CurrentAccount account);

    Optional<CurrentAccount> findByAccNumber(String accNumber);
}
