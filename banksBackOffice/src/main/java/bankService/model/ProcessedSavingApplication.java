package bankService.model;

import bankService.model.enums.CaseStatus;
import sharedData.model.SavingApplication;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Entity
public class ProcessedSavingApplication implements ProcessedApplication<SavingApplication>{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne
    @JoinColumn(name = "CUSTOMER_ID")
    @NotNull
    private CustomerRecord customerRecord;

    @ManyToOne
    @JoinColumn(name = "ADVISOR_ID")
    private AdvisorEntity advisor;

    @Enumerated(EnumType.STRING)
    @NotNull
    private CaseStatus status;

    @ManyToOne
    @JoinColumn(name = "SAVING_APP_ID")
    @NotNull
    private SavingApplication productApp;

    public Long getId() {
        return id;
    }

    @Override
    public CustomerRecord getCustomerRecord() {
        return customerRecord;
    }

    public void setCustomerRecord(CustomerRecord customerRecord) {
        this.customerRecord = customerRecord;
    }

    public AdvisorEntity getAdvisor() {
        return advisor;
    }

    public void setAdvisor(AdvisorEntity advisor) {
        this.advisor = advisor;
    }

    public CaseStatus getStatus() {
        return status;
    }

    public void setStatus(CaseStatus status) {
        this.status = status;
    }
    @Override
    public SavingApplication getProductApp() {
        return productApp;
    }

    public void setProductApp(SavingApplication productApp) {
        this.productApp = productApp;
    }

}
