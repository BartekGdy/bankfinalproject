package bankService.model;

import sharedData.model.enums.ProductType;

import javax.persistence.Entity;

@Entity
public class SavingAccount extends Holding{


    private Double interestsRate;

    public Double getInterestsRate() {
        return interestsRate;
    }

    public void setInterestsRate(Double interestsRate) {
        this.interestsRate = interestsRate;
    }

    public Long getId() {
        return super.getId();
    }

    public void setId(Long id) {
        super.setId(id);
    }

    public ProductType getProductType() {
        return super.getProductType();
    }

    public void setProductType(ProductType productType) {
        super.setProductType(productType);
    }

    public String getProductNumber() {
        return super.getProductNumber();
    }

    public void setProductNumber(String productNumber) {
        super.setProductNumber(productNumber);
    }
}
