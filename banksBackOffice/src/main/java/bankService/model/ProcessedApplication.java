package bankService.model;

import sharedData.model.SavingApplication;

/**
 * Created by RENT on 2017-11-07.
 */
public interface ProcessedApplication<App> {


    CustomerRecord getCustomerRecord();

    App getProductApp();
}
