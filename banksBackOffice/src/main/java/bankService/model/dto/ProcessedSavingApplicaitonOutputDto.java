package bankService.model.dto;

import bankService.model.enums.CaseStatus;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import sharedData.model.enums.ProductType;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

@ApiModel("Case ID")
public class ProcessedSavingApplicaitonOutputDto implements Serializable{

    @ApiModelProperty("Case ID")
    @JsonProperty(value = "id")
    private Long id;

    @ApiModelProperty("Customer ID")
    @JsonProperty(value = "customer_id")
    private Long customerId;

    @ApiModelProperty("Advisor")
    @JsonProperty(value = "advisor_username")
    private String advisorUservname;

    @ApiModelProperty("Status")
    @JsonProperty(value = "status")
    private CaseStatus status;

    @ApiModelProperty("Product")
    @JsonProperty(value = "product")
    private ProductType productType;

    @ApiModelProperty("Application")
    @JsonProperty(value = "application_id")
    private Long productAppId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getCustomerId() {
        return customerId;
    }

    public void setCustomerId(Long customerId) {
        this.customerId = customerId;
    }

    public String getAdvisorUservname() {
        return advisorUservname;
    }

    public void setAdvisorUservname(String advisorUservname) {
        this.advisorUservname = advisorUservname;
    }

    public CaseStatus getStatus() {
        return status;
    }

    public void setStatus(CaseStatus status) {
        this.status = status;
    }

    public ProductType getProductType() {
        return productType;
    }

    public void setProductType(ProductType productType) {
        this.productType = productType;
    }

    public Long getProductAppId() {
        return productAppId;
    }

    public void setProductAppId(Long productAppId) {
        this.productAppId = productAppId;
    }
}
