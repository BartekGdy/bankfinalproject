package bankService.model.dto;

import bankService.model.enums.AccState;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import sharedData.model.enums.ProductType;

import java.io.Serializable;

@ApiModel("Current Account")
public class CurrentAccountDto implements Serializable {

    @ApiModelProperty(value = "Product Type")
    @JsonProperty("productType")
    private ProductType productType;

    @ApiModelProperty(value = "Product Number")
    @JsonProperty("productNumber")
    private String productNumber;

    @ApiModelProperty(value = "Product State")
    @JsonProperty("accState")
    private AccState state;

    @ApiModelProperty(value = "Overdraft Amount")
    @JsonProperty("overdraftAmount")
    private Double overdraftAmount;

    public ProductType getProductType() {
        return productType;
    }

    public void setProductType(ProductType productType) {
        this.productType = productType;
    }

    public String getProductNumber() {
        return productNumber;
    }

    public void setProductNumber(String productNumber) {
        this.productNumber = productNumber;
    }

    public AccState getState() {
        return state;
    }

    public void setState(AccState state) {
        this.state = state;
    }

    public Double getOverdraftAmount() {
        return overdraftAmount;
    }

    public void setOverdraftAmount(Double overdraftAmount) {
        this.overdraftAmount = overdraftAmount;
    }
}
