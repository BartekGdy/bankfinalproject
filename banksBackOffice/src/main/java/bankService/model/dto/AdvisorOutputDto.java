package bankService.model.dto;

import bankService.model.AdvisorEntity;
import bankService.model.enums.Role;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateDeserializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateSerializer;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.springframework.stereotype.Component;
import sharedData.mapper.EntityMapper;

import javax.persistence.GeneratedValue;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.time.LocalDate;

@ApiModel("AdvisorOutput")
public class AdvisorOutputDto implements Serializable{

    @ApiModelProperty(value = "username")
    @JsonProperty("username")
    @NotNull
    @Size(min = 8,max = 24)
    private String username;
    @ApiModelProperty(value = "password")
    @JsonProperty("password")
    @NotNull
    @Size(min = 8,max = 24)
    private String password;

    @ApiModelProperty(value = "Role")
    @JsonProperty("role")
    @NotNull
    private Role role;
    @ApiModelProperty(value = "Name")
    @JsonProperty("firstname")
    @NotNull
    @Size(min = 2,max = 30)
    private String firstname;
    @ApiModelProperty(value = "Surname")
    @JsonProperty("lastname")
    @NotNull
    @Size(min = 2,max = 30)
    private String lastname;
    @ApiModelProperty(value = "DOB",dataType = "java.lang.String", example = "01/01/2001")
    @JsonProperty("DOB")
    @JsonFormat(pattern = "dd/MM/yyyy")
    @JsonDeserialize(using = LocalDateDeserializer.class)
    @JsonSerialize(using = LocalDateSerializer.class)
    private LocalDate dateOfBirth;

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public LocalDate getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(LocalDate dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }
}
