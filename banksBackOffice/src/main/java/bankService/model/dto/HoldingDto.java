package bankService.model.dto;

import bankService.model.CustomerRecord;

import bankService.model.enums.AccState;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import sharedData.model.enums.ProductType;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@ApiModel("Holding List")
public class HoldingDto{

    @ApiModelProperty(value = "Product Type")
    @JsonProperty("productType")
    private ProductType productType;

    @ApiModelProperty(value = "Product Number")
    @JsonProperty("productNumber")
    private String productNumber;

    @ApiModelProperty(value = "Product State")
    @JsonProperty("accState")
    private AccState state;

    public ProductType getProductType() {
        return productType;
    }

    public void setProductType(ProductType productType) {
        this.productType = productType;
    }

    public String getProductNumber() {
        return productNumber;
    }

    public void setProductNumber(String productNumber) {
        this.productNumber = productNumber;
    }

    public AccState getState() {
        return state;
    }

    public void setState(AccState state) {
        this.state = state;
    }
}
