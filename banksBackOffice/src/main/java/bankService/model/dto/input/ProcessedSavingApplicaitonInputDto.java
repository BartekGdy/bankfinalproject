package bankService.model.dto.input;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

@ApiModel("Case ID")
public class ProcessedSavingApplicaitonInputDto implements Serializable{


    @ApiModelProperty("Customer ID")
    @JsonProperty(value = "customer_id")
    @NotNull
    private Long customerId;

    @ApiModelProperty("Application")
    @JsonProperty(value = "application_id")
    @NotNull
    private Long productAppId;

    public Long getCustomerId() {
        return customerId;
    }

    public void setCustomerId(Long customerId) {
        this.customerId = customerId;
    }

    public Long getProductAppId() {
        return productAppId;
    }

    public void setProductAppId(Long productAppId) {
        this.productAppId = productAppId;
    }
}
