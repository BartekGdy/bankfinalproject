package bankService.model.dto.input;

import bankService.model.dto.CustRecordAddressDto;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateDeserializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateSerializer;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotEmpty;
import sharedData.model.enums.Gender;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.List;

/**
 * Created by RENT on 2017-11-06.
 */
@ApiModel("Customer Record")
public class CustomerRecordInputDto implements Serializable{
    @ApiModelProperty(value = "Name")
    @JsonProperty("firstname")
    @NotEmpty
    private String firstname;
    @ApiModelProperty(value = "Surname")
    @JsonProperty("lastname")
    @NotEmpty
    private String lastname;
    @ApiModelProperty(value = "DOB",dataType = "java.lang.String", example = "01/01/2001")
    @JsonProperty("DOB")
    @JsonFormat(pattern = "dd/MM/yyyy")
    @JsonDeserialize(using = LocalDateDeserializer.class)
    @JsonSerialize(using = LocalDateSerializer.class)
    private LocalDate dateOfBirth;
    @ApiModelProperty(value = "Gender")
    @JsonProperty("gender")
    @NotNull
    private Gender gender;
    @ApiModelProperty(value = "Email")
    @JsonProperty("email")
    @NotEmpty
    @Email
    private String email;
    @ApiModelProperty(value = "CustomerRecordAddress")
    @JsonProperty("addresses")
    @NotNull
    private List<CustRecordAddressDto> addresses;
    @ApiModelProperty(value = "Phone_Number")
    @JsonProperty("phone")
    @NotEmpty
    private String phone;

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public LocalDate getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(LocalDate dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public Gender getGender() {
        return gender;
    }

    public void setGender(Gender gender) {
        this.gender = gender;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public List<CustRecordAddressDto> getAddresses() {
        return addresses;
    }

    public void setAddresses(List<CustRecordAddressDto> addresses) {
        this.addresses = addresses;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }
}
